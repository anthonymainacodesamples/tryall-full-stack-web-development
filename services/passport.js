/**
 * Created by AnthonyMaina on 9/28/17.
 */

const passport  = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser ((user, done) => {
    done(null, user.id);
});

passport.deserializeUser((id, done)=> {
    User.findById(id)
    .then((user) => {
    done(null, user);
    });
});

passport.use(new GoogleStrategy({
        clientID:keys.googleClientID,
        clientSecret:keys.googleClientSecret,
        callbackURL:'/auth/google/callback',
        proxy:true
    }, async (accessToken, refreshToken, profile, done) => {

    const existingUser = await User.findOne({ googleId: profile.id});

    if(existingUser) {
        if(!existingUser.emailAddress){
            const emailAddress = profile.emails[0].value;
            User.findOneAndUpdate({googleId: profile.id},{
               emailAddress
            }).exec();
            return done(null, existingUser);
        } else {
            //User exists
            return done(null, existingUser);
        }
    }
        const user = await new User({
            googleId: profile.id,
            displayName: profile.displayName
        }).save();

        done(null, user);
    })
);
