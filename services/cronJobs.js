let CronJob = require('cron').CronJob;
const logger = require('./logger');
const fs = require('async-file');

const mongoose = require('mongoose');

const Trial = mongoose.model('tryallcompanies');
const ReminderRecord = mongoose.model('reminder_records');
const BroadCategory = mongoose.model("broad-categories");
const SearchPageClicks = mongoose.model("search_page_clicks_data");
const SearchQueries = mongoose.model("search_query_data");
const SimilarTrials = mongoose.model("similarcompanies");
const UnsubReasons = mongoose.model("unsub_reasons");
const User = mongoose.model('users');
const Category = mongoose.model("category");

const admin = require("firebase-admin");
var s3 = require('s3');

var client = s3.createClient({
  s3Options: {
    accessKeyId: "AKIAJO77AU3AGVDEIGEA",
    secretAccessKey: "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",
    region: "us-east-1"
  },
});


let random15TenTryalls = [];
let random30TenTryalls = [];

/**
 * Method for uploading collection to S3
 * @param filePath - path to json file with collection data
 * @param keyName - name for identifying collection in S3
 * @param date - time that data was saved. Also is the name of the directory
 */
uploadToS3 = (filePath, keyName, date) => {

  let params = {
    localFile: filePath,

    s3Params: {
      Bucket: "tryall-mongodb-backups",
      Key: `${date}/${keyName}.json`
    },
  };


  var uploader = client.uploadFile(params);
  uploader.on('error', function(err) {
    logger.error(`unable to upload: ${err.stack}`);
  });
  /*uploader.on('progress', function() {
    logger.info(`progress: ${uploader.progressMd5Amount},
      ${uploader.progressAmount}, ${uploader.progressTotal}`);
  });*/
  uploader.on('end', function() {
    logger.info(`Done uploading ${keyName}`);
  });

};

/**
 * Job that backs up all mongoDB data in S3
 */
let backupMongodbData = new CronJob('0 0 0 * * * *', async function () {


  try {
    let broadCategories = await BroadCategory.find();
    let categories = await Category.find();
    let reminderRecords = await ReminderRecord.find();
    let searchPageClicks = await SearchPageClicks.find();
    let searchQueries = await SearchQueries.find();
    let similarTrials = await SimilarTrials.find();
    let trials = await Trial.find();
    let unsubReasons = await UnsubReasons.find();
    let users = await User.find();

    await fs.writeFile("mongodb-backups/broadCategories.json", broadCategories, "utf8");
    await fs.writeFile("mongodb-backups/categories.json", categories, "utf8");
    await fs.writeFile("mongodb-backups/reminderRecords.json", reminderRecords, "utf8");
    await fs.writeFile("mongodb-backups/searchPageClicks.json", searchPageClicks, "utf8");
    await fs.writeFile("mongodb-backups/searchQueries.json", searchQueries, "utf8");
    await fs.writeFile("mongodb-backups/similarTrials.json", similarTrials, "utf8");
    await fs.writeFile("mongodb-backups/trials.json", trials, "utf8");
    await fs.writeFile("mongodb-backups/unsubReasons.json", unsubReasons, "utf8");
    await fs.writeFile("mongodb-backups/users.json", users, "utf8");

    logger.info("Saved all collections to file");

    let now = new Date();

    uploadToS3("mongodb-backups/broadCategories.json", "broadCategories", now.toDateString());
    uploadToS3("mongodb-backups/categories.json", "categories", now.toDateString());
    uploadToS3("mongodb-backups/reminderRecords.json", "reminderRecords", now.toDateString());
    uploadToS3("mongodb-backups/searchPageClicks.json", "searchPageClicks", now.toDateString());
    uploadToS3("mongodb-backups/searchQueries.json", "searchQueries", now.toDateString());
    uploadToS3("mongodb-backups/similarTrials.json", "similarTrials", now.toDateString());
    uploadToS3("mongodb-backups/trials.json", "trials", now.toDateString());
    uploadToS3("mongodb-backups/unsubReasons.json", "unsubReasons", now.toDateString());
    uploadToS3("mongodb-backups/users.json", "users", now.toDateString());

  } catch (err) {
    logger.error(`Error getting mongodb info - ${err}`)
  }


}, null, true);

let get15randomTryallsJob = new CronJob('0 0 0 * * * *', function() {

  Trial.aggregate(
    [
      {$match: {
        tagLine: {
          $ne: ""
        },
        description: {
          $ne: "",
          $exists: true
        },
        monthlyDollarPrice: {
          $nin: [0, 0.1, "0", "0.1"]
        },
        DurationInDays: {
          $ne: 0
        }
        }},
      {$sample: {
        size: 15
      }}
    ], function (err, res) {
      if(err){
        console.log(`'err: ${err}`);
        return;
      }
      /*res.forEach(item => {
        console.log(`cron: ${item.monthlyDollarPrice} and name: ${item.name}`)
      });*/
      random15TenTryalls = res;

    }
  );
}, null, true);

let get30randomTryallsJob = new CronJob('0 0 0 * * * *', function() {

  Trial.aggregate(
    [
      {$match: {
          tagLine: {
            $ne: ""
          },
          description: {
            $ne: "",
            $exists: true
          },
          monthlyDollarPrice: {
            $nin: [0, 0.1, "0", "0.1"]
          },
          DurationInDays: {
            $ne: 0
          }
        }},
      {$sample: {
          size: 30
        }}
    ], function (err, res) {
      if(err){
        console.log(`'err: ${err}`);
        return;
      }
      random30TenTryalls = res;

    }
  );
}, null, true);

/**
 * Job that runs every day and searches for all reminders that have their end dates equal to
 * the current day. It then saves all the reminders into the specific user's past trials object
 */
let getTrialsEndingTodayJob = new CronJob('0 0 0 * * *', async function () {

  let start = new Date();
  start.setHours(0, 0, 0, 0);

  let end = new Date();
  end.setHours(23, 59, 59, 999);

  /*let categories = await User.findOne({"_id": "59ce5ee130432151b6fb1368"}).populate("followedCategories").exec();
  console.log(categories.followedCategories);*/

  /*const payload = {
    notification: {
      title: `${trial.name} Free Trial Ending!`,
      body: `Your ${trial.DurationInDays} day free trial for ${trial.name} ends today`,
      imageUrl: trial.logoLink
    }
  };

  admin.messaging().sendToDevice("f1qmh2xifUQ:APA91bEj1kHuxHbqgclmZOj1RoeJBwI1xQ2553JE1qJsy0HOSJsH56vxj_zFOnwhUd5SbD3YV9RZeV158jKK80Y6rKqBwT_rZVn-Q061wKMPOKFTsrpUOu8uPk7aHiHyuI_ILfz6cqkF", payload)
    .then(function (response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function (error) {
      console.log("Error sending message:", error);
    });*/


  try{
    let remindersForToday = await ReminderRecord.find(
      {'activeTrials.endDate': {$gte: start, $lt: end}},
      (err, res) => {
        let reminders = [];

        res.forEach((reminder) => {
          reminders.push(reminder);
        });

        return reminders;
      });

    if(remindersForToday){
      // console.log(`reminders for today: ${remindersForToday}`);

      remindersForToday.forEach(async (reminder) => {
        let items = [];
        reminder.activeTrials.forEach((item) => {
          let toAdd = {
            "trialId": item.trialId,
            "userIf": item.userId
        };
          items.push(toAdd)
        });

        items.forEach((item) => {
          let trial = Trial.findOne({"_id": item.trialId});
          // Add trial to user's past trials
          try{
            User.findOneAndUpdate(item.userId,
              {
                pastTrials:trial
              })
          }catch (err){
            console.log(err);
          }

        });

      });
    }
  }catch(err){
    logger.error(`error in job for saving past reminders: ${err}`)
  }

}, null, true);

let getRemindersForTheDayJob = new CronJob('0 0 0 * * *', async function () {

  let start = new Date();
  start.setHours(0, 0, 0, 0);

  let end = new Date();
  end.setHours(23, 59, 59, 999);

  /*let categories = await User.findOne({"_id": "59ce5ee130432151b6fb1368"}).populate("followedCategories").exec();
  console.log(categories.followedCategories);*/

  /*const payload = {
    notification: {
      title: `${trial.name} Free Trial Ending!`,
      body: `Your ${trial.DurationInDays} day free trial for ${trial.name} ends today`,
      imageUrl: trial.logoLink
    }
  };

  admin.messaging().sendToDevice("f1qmh2xifUQ:APA91bEj1kHuxHbqgclmZOj1RoeJBwI1xQ2553JE1qJsy0HOSJsH56vxj_zFOnwhUd5SbD3YV9RZeV158jKK80Y6rKqBwT_rZVn-Q061wKMPOKFTsrpUOu8uPk7aHiHyuI_ILfz6cqkF", payload)
    .then(function (response) {
      console.log("Successfully sent message:", response);
    })
    .catch(function (error) {
      logger.error("Error sending message:", error);
    });*/


  try{
    let remindersForToday = await ReminderRecord.find(
      {'activeTrials.reminderDate': {$gte: start, $lt: end}},
      (err, res) => {
        let reminders = [];

        res.forEach((reminder) => {
          reminders.push(reminder);
        });

        return reminders;
      });

    if(remindersForToday){
      // console.log(`reminders for today: ${remindersForToday}`);

      remindersForToday.forEach(async (reminder) => {
        let items = [];
        reminder.activeTrials.forEach((item) => {
          let toAdd = {
            "trialId": item.trialId,
            "userIf": item.userId
          };
          items.push(toAdd)
        });

        items.forEach((item) => {
          let trial = Trial.findOne({"_id": item.trialId});
          const payload = {
            notification: {
              title: `${trial.name} Free Trial Ending!`,
              body: `Your ${trial.DurationInDays} day free trial for ${trial.name} is ending soon!`,
              imageUrl: trial.logoLink
            }
          };

          admin.messaging().sendToDevice(reminder.deviceId, payload)
            .then(function (response) {
              console.log("Successfully sent message:", response);

              // Add trial to user's past trials
              try{
                User.findOneAndUpdate(item.userId,
                  {
                    pastTrials:trial
                  })
              }catch (err){
                logger.error(err);
              }

            })
            .catch(function (error) {
              logger.error(`Error sending message: ${error}`);
            });
        });

      });
    }
  }catch(err){
    logger.error(`error in job for sending reminders: ${err}`)
  }

}, null, true);

module.exports = {
  get15randomTryalls: () => {
    return random15TenTryalls
  },
  get30randomTryalls: () => {
    return random30TenTryalls
  }
};