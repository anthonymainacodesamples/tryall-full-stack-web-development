/**
 * Created by AnthonyMaina on 3/2/18.
 */

const keys = require('../../config/keys');
const moment = require('moment');

module.exports = (matchFound) => {

    const option1 = moment(matchFound.options.option1).utcOffset('-0500').format("dddd, MMMM Do YYYY, h:mm:ss a");
    const option2 = moment(matchFound.options.option2).utcOffset('-0500').format("dddd, MMMM Do YYYY, h:mm:ss a");
    const option3 = moment(matchFound.options.option3).utcOffset('-0500').format("dddd, MMMM Do YYYY, h:mm:ss a");
    const additionalInfo = matchFound.additionalInfo;

    return `
    <html>
        <body>
            <div>
            
                <h3>${matchFound.title}</h3>
                <p>${matchFound.body}</p>
                <div  style='text-align: center'>
                <a href="${keys.redirectDomain}/tester-confirm/${additionalInfo.trialId}/${additionalInfo.testerId}/${additionalInfo.userId}/option1"> ${option1}</a><br/>
                <a href="${keys.redirectDomain}/tester-confirm/${additionalInfo.trialId}/${additionalInfo.testerId}/${additionalInfo.userId}/option2"> ${option2}</a><br/>
                <a href="${keys.redirectDomain}/tester-confirm/${additionalInfo.trialId}/${additionalInfo.testerId}/${additionalInfo.userId}/option3"> ${option3}</a>
                </div>
            </div>
        </body>
    </html>
  `;
};