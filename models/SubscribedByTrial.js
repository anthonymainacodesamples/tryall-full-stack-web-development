/**
 * Created by AnthonyMaina on 3/7/18.
 */
const mongoose =  require('mongoose');
mongoose.Promise = global.Promise;
const User = require('./User');
const Trial = require('./Trial');
const { Schema }  = mongoose;


const subscriptionsPerUserSchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    subscribedTrials:[
        {
            _id:false,
            trialId: { type: Schema.Types.ObjectId, ref: Trial }
        }
    ]
});

mongoose.model('user-subscriptions', subscriptionsPerUserSchema);