/**
 * Created by AnthonyMaina on 2/28/18.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const User = require('./User');
const Trial = require('./Trial');
const { Schema }  = mongoose;


const activeEligibilitySchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    trialsInfo: [
        {
            _id:false,
        trialId: { type: Schema.Types.ObjectId, ref: Trial },
        dateSubscribed: {type: Date},
        duration: { type: Number }
        }
    ]
});

mongoose.model("active_eligibility_items", activeEligibilitySchema);