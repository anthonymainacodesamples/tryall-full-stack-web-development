/**
 * Created by AnthonyMaina on 10/27/17.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Trial = require('./Trial');
const { Schema }  = mongoose;


const SimilarTrialsSchema = new Schema({
    similar_tryalls:[
        {
            id:{type: Schema.Types.ObjectId, ref: Trial},
            cosine_sim: Number
        }
    ]

});

mongoose.model('similarcompanies', SimilarTrialsSchema);

