/**
 * Created by AnthonyMaina on 9/29/17.
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;
const Trial = require('./Trial');
const BroadCategory = require('./BroadCategory');


const userSchema = new Schema ({
    googleId:String,
    displayName:String,
    emailAddress:String,
    savedTrials:[{type: Schema.Types.ObjectId, ref:Trial}],
    activeTrials:[{type: Schema.Types.ObjectId, ref:Trial}],
    pastTrials:[{type: Schema.Types.ObjectId, ref:Trial}],
    testingTrials:[{type: Schema.Types.ObjectId, ref:Trial}],
    followedCategories: [{type: Schema.Types.Object, ref:"broad-categories"}],
    remindersDays:[Number],
    highFrequency:{type:Boolean, default: false},
    webPushSubscription:{},
    surveyResponded:{type:Boolean, default:false},
    preferences:{
        sevenDays:{type:Boolean, default: false},
        fiveDays:{type:Boolean, default: false},
        threeDays:{type:Boolean, default: false},
        oneDay:{type:Boolean, default: false}
    },
    registrationToken: String
});

mongoose.model('users', userSchema);