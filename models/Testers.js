/**
 * Created by AnthonyMaina on 2/20/18.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Trial = mongoose.model('tryallcompanies');
const User = mongoose.model('users');
const { Schema }  = mongoose;


const TesterSchema = new Schema({
    trialId:{type: Schema.Types.ObjectId, ref:Trial},
    testers:[
        {
            userId: {type: Schema.Types.ObjectId, ref:User},
            userName: {type:String},
            _id:false
        }
    ]
});

mongoose.model('testers-list', TesterSchema);
