/**
 * Created by AnthonyMaina on 10/30/17.
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Trial = require('./Trial');
const User = require('./User');
const { Schema }  = mongoose;


const ReminderRecordSchema = new Schema({
    userId:{type: Schema.Types.ObjectId, ref:User},
    deviceId:String,
    activeTrials:[
        {
            _id:false,
            trialId: {type: Schema.Types.ObjectId, ref: Trial},
            dateSubscribed: Date,
            reminderDate: Date,
            endDate: Date
        }
    ]
});

mongoose.model('reminder_records', ReminderRecordSchema);