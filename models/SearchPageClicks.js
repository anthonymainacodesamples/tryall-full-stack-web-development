/**
 * Created by AnthonyMaina on 10/29/17.
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;
const Trial = require('./Trial');
const User = require('./User');


const searchPageClicksSchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    trialId:{type: Schema.Types.ObjectId, ref:Trial},
    dateTimeSearched:Date
});

mongoose.model('search_page_clicks_data', searchPageClicksSchema);