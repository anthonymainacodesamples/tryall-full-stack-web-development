const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Trial = require('./Trial');
const { Schema }  = mongoose;


const CategorySchema = new Schema({
    id: Schema.Types.ObjectId,
    value: String
});

mongoose.model('category', CategorySchema);

