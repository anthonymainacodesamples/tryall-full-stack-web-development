/**
 * Created by AnthonyMaina on 3/7/18.
 */


const mongoose =  require('mongoose');
mongoose.Promise = global.Promise;
const User = require('./User');
const Trial = require('./Trial');
const { Schema }  = mongoose;


const clicksPerUserSchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    clickedTrials:[
        {
            _id:false,
            trialId: { type: Schema.Types.ObjectId, ref: Trial },
            dateClicked:Date
        }
    ]

});

mongoose.model('clicks_per_users', clicksPerUserSchema);