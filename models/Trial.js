/**
 * Created by AnthonyMaina on 10/17/17.
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;


const trialSchema = new Schema ({
    name: String,
    tagLine: String,
    description: String,
    websiteLink: String,
    freeTrialPage:String,
    DurationInDays: Number,
    conditionalDuration: String,
    monthlyDollarPrice: {type: Number, default: 0},
    conditionalPricing: String,
    requiresCreditCard: Boolean,
    logoLink: String,
    categories: [],
    filters:[],
    city: String,
    countryCode: String,
    region: String,
    stateCode: String,
    similar: []
});

mongoose.model('tryallcompanies', trialSchema);