/**
 * Created by AnthonyMaina on 10/29/17.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;
const User = require('./User');


const searchQuerySchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    searchQuery:String,
    dateTimeSearched:Date
});

mongoose.model('search_query_data', searchQuerySchema);