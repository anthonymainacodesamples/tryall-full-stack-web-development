/**
 * Created by AnthonyMaina on 12/5/17.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;
const User = require('./User');
const Trial = require('./Trial');


const unsubReasonSchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    trialId: {type: Schema.Types.ObjectId, ref:Trial},
    dateForReason:Date,
    reasonProvided:{type:String}
});

mongoose.model('unsub_reasons', unsubReasonSchema);
