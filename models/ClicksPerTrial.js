/**
 * Created by AnthonyMaina on 3/7/18.
 */


const mongoose =  require('mongoose');
mongoose.Promise = global.Promise;
const User = require('./User');
const Trial = require('./Trial');
const { Schema }  = mongoose;


const clicksPerTrialSchema = new Schema ({
    trialId:{type: Schema.Types.ObjectId, ref:Trial},
    clickedUsersInfo:[
        {
            _id:false,
            userId: { type: Schema.Types.ObjectId, ref: User },
            dateClicked:Date
        }
    ]

});

mongoose.model('clicks_per_trials', clicksPerTrialSchema);