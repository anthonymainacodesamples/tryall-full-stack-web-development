/**
 * Created by AnthonyMaina on 3/2/18.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Trial = require('./Trial');
const User = require('./User');
const { Schema }  = mongoose;

const MatchScheduleSchema = new Schema({
    userId:{type: Schema.Types.ObjectId, ref:User},
    testerId:{type: Schema.Types.ObjectId, ref:User},
    trialId:{type: Schema.Types.ObjectId, ref:Trial},
    confirmedTimeStatus:Boolean,
    matchRejected:Boolean,
    conversationComplete:Boolean,
    confirmedTime:String,
    emailSent:Boolean,
    suggestedOptions:{}
});

mongoose.model('match_schedule_records', MatchScheduleSchema);