const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;


const BroadCategorySchema = new Schema({
    id: Schema.Types.ObjectId,
    name: String,
    browse_icon:String,
    sub_categories: []
});

mongoose.model('broad-categories', BroadCategorySchema);

