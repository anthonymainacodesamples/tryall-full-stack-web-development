/**
 * Created by AnthonyMaina on 2/14/18.
 */
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;


const FilterSchema = new Schema({
    name: String
});

mongoose.model('filters', FilterSchema);