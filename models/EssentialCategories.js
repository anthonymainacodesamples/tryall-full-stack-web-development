/**
 * Created by AnthonyMaina on 2/13/18.
 */


const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const BroadCategory = mongoose.model('broad-categories');

const { Schema }  = mongoose;

const essentialCategorySchema = new Schema ({
    categoryId:{type: Schema.Types.ObjectId, ref:BroadCategory},
    name:String
});

mongoose.model("essential_categories", essentialCategorySchema);