/**
 * Created by AnthonyMaina on 2/7/18.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const { Schema }  = mongoose;

const questionSchema = new Schema ({
    prompt: String,
    options:
        [
            {
                orderNum:{type:Number},
                response:{type:String},
                published:{type:Boolean},
                linkedCategories:[],
                linkedFilters:[]
            }
        ]

});

mongoose.model("onboarding_questions", questionSchema);