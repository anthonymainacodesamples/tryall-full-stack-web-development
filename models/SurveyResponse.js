/**
 * Created by AnthonyMaina on 2/7/18.
 */

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const User = require('./User');
const SurveyQuestion = require('./SurveyQuestion');
const { Schema }  = mongoose;

const responseSchema = new Schema ({
    userId:{type: Schema.Types.ObjectId, ref:User},
    responses: [
        {
            _id : false,
            questionId:{type: Schema.Types.ObjectId,ref:SurveyQuestion},
            responseId:{type: Schema.Types.ObjectId}
        }
    ]

});

mongoose.model("onboarding_responses", responseSchema);