# Tryall Full Stack Web Development README #

Code repository by Anthony Maina

### Languages ###
Javascript(ES6), Python

### Quick summary 
Flagship product by the Asili Labs team to allow individuals and small businesses to test out subscription based products.
This beta version allows google based authentication, browsing of free trials by category, filtering by preferences and built in scheduling and messaging between users and testers of the same product.

### Front End ###
The front end is built entirely in react with redux handling state management and bootstrap being incorporated in various pages.

### Back End ###
The back end is a nodejs/express server that handles all the api calls from both the web front end and the iOS mobile app with the database currently in MongoDB.


### Version ###
Beta

### How do I get set up? ###

To get set up run npm run dev in the main directory.

