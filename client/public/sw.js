/**
 * Created by AnthonyMaina on 12/10/17.
 */
self.addEventListener('push', function(event) {
    if (event.data) {
        const notifObject = event.data.text();

        const title = 'Free Trial Reminder';
        const options = {
            body: `Your ${notifObject} free trial is ending soon.`,
            icon: 'images/finalNotifLogo.png'

        };
        event.waitUntil(self.registration.showNotification(title, options));
    } else {
        console.log('This push event has no data.');
    }
});