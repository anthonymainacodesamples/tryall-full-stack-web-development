/**
 * Created by AnthonyMaina on 10/5/17.
 */

export const FETCH_USER = 'fetch_user';
export const FETCH_RESULTS = 'fetch_results';
export const FETCH_RECOMMENDED = 'fetch_recommended';
export const FETCH_TRIAL = 'fetch_trial';
export const SAVE_TRIAL = 'save_trial';
export const FETCH_SAVED ='fetch_saved';
export const REMOVE_SAVED = 'remove_saved';
export const FETCH_SETTINGS = 'fetch_settings';
export const SEARCH_LOGGING = 'search_logging';
export const FETCH_SIMILAR = 'fetch_similar';
export const SAVE_REMINDER = 'save_reminder';
export const SAVE_ACTIVE = 'save_active';
export const FETCH_ACTIVE ='fetch_active';
export const FETCH_PAST ='fetch_past';
export const FETCH_FEATURED = 'fetch_featured';
export const FETCH_CATEGORIES = 'fetch_categories';
export const SAVE_CATEGORY = 'save_category';
export const REMOVE_CATEGORY = 'remove_category';
export const UPDATE_REMINDERS = 'update_reminders';
export const UPDATE_FREQUENCY = 'update_frequency';
export const POST_UNSUB = 'post_unsub';
export const FETCH_SINGLE_CATEGORY = 'fetch_single_category';
export const FETCH_TRIALS_BY_CATEGORY = 'fetch_trials_by_category';
export const SAVE_NOTIFICATION_SUBSCRIPTION = 'save_notification_subscription';
export const FETCH_SELECTED_CATEGORIES = 'fetch_selected_categories';
export const FETCH_NOTIFICATION_STATE = 'fetch_notification_state';

//Beta V2
export const RETRIEVE_QUESTIONS = 'retrieve_questions';
export const SUBMIT_SURVEY = 'submit_survey';
export const RETRIEVE_CUSTOM = 'retrieve_custom';
export const RETRIEVE_RESPONSES = 'retrieve_responses';
export const BECOME_TESTER = 'become_tester';
export const FIND_TESTER = 'find_tester';
export const CHECK_ELIGIBILITY = 'is_eligible';
export const CONNECT_REQUEST = 'connect_request';
export const CONFIRM_TIME = 'confirm_time';
export const FETCH_MATCHES = 'fetch_matches';
export const REJECT_MATCH = 'reject_match';



// Data Actions Beta V2
export const DATA_CLICKS = 'data_clicks';