/**
 * Created by AnthonyMaina on 10/5/17.
 */

import axios from 'axios';

import {
    FETCH_USER,
    FETCH_RESULTS,
    FETCH_RECOMMENDED,
    FETCH_TRIAL,
    SAVE_TRIAL,
    FETCH_SAVED,
    REMOVE_SAVED,
    FETCH_SETTINGS,
    SEARCH_LOGGING,
    FETCH_SIMILAR,
    SAVE_REMINDER,
    SAVE_ACTIVE,
    FETCH_ACTIVE,
    FETCH_PAST,
    FETCH_FEATURED,
    FETCH_CATEGORIES,
    SAVE_CATEGORY,
    REMOVE_CATEGORY,
    UPDATE_REMINDERS,
    UPDATE_FREQUENCY,
    POST_UNSUB,
    FETCH_SINGLE_CATEGORY,
    FETCH_TRIALS_BY_CATEGORY,
    SAVE_NOTIFICATION_SUBSCRIPTION,
    FETCH_SELECTED_CATEGORIES,
    FETCH_NOTIFICATION_STATE,
    RETRIEVE_QUESTIONS,
    SUBMIT_SURVEY,
    RETRIEVE_CUSTOM,
    RETRIEVE_RESPONSES,
    BECOME_TESTER,
    FIND_TESTER,
    CHECK_ELIGIBILITY,
    CONNECT_REQUEST,
    CONFIRM_TIME,
    FETCH_MATCHES,
    REJECT_MATCH,
    DATA_CLICKS
    }
    from './types';

export const fetchUser = () => async dispatch => {
    const res = await axios.get('/api/current_user');
    dispatch({ type: FETCH_USER, payload: res.data});
};

export const fetchResults = (term) => async dispatch => {
  const res = await axios.post('/search', {searchTerm:term});
  dispatch({ type: FETCH_RESULTS, payload: res.data });
};

export const fetchRecommended = (summarized) => async dispatch => {

    const res = await axios.get('/api/trials/recommended',{
        params: {
            summarizedVal: summarized
        }
    });
    dispatch({ type: FETCH_RECOMMENDED, payload: res.data});
};

export const fetchTrial = (id) => async dispatch => {
  const res = await axios.get(`/api/trials/${id}`);
    dispatch({type: FETCH_TRIAL, payload:res.data });
};

export const saveTrial = (id) => async dispatch => {
  const res = axios.post(`/api/trials/saved/${id}`);
    dispatch({type: SAVE_TRIAL, payload: res.data});
};

export const fetchSaved = (id) => async dispatch => {
    const res = await axios.get('/api/trials/saved');
    dispatch({type: FETCH_SAVED, payload: res.data});
};

export const removeFromSaved = (id) => async dispatch => {
    const res = await axios.delete(`/api/trials/saved/${id}`);
    dispatch({type:REMOVE_SAVED, payload:res.data});
};

export const fetchSettings = () => async dispatch => {
    const res = await axios.get('/api/profile/settings');
    dispatch({type:FETCH_SETTINGS, payload:res.data});
};

export const searchLogging = (query) => async dispatch => {
    const res = await axios.post('/api/data/search_queries', { searchQuery:query });
    dispatch({type:SEARCH_LOGGING, payload:res.data});
};

export const fetchSimilar = (id) => async dispatch => {
    const res = await axios.get(`/api/similar/${id}`);
    dispatch({type:FETCH_SIMILAR, payload:res.data});
};

export const saveReminder = (trialId,dateSubscribed) => async dispatch => {
  const res = await axios.post('/api/reminder/save', { trialId,dateSubscribed } );
  dispatch({type:SAVE_REMINDER, payload:res.data});
};

export const saveActiveTrial = (id, dateSubscribed, duration) => async dispatch => {
    const res = axios.post('/api/trials/active', {trialId: id, dateSubscribed, duration});
    dispatch({type: SAVE_ACTIVE, payload: res.data});
};

export const fetchActive = () => async dispatch => {
    const res = await axios.get('/api/trials/active');
    dispatch({type: FETCH_ACTIVE, payload: res.data});
};

export const fetchPast = () => async dispatch => {
    const res = await axios.get('/api/trials/past');
    dispatch({type: FETCH_PAST, payload: res.data});
};

export const fetchFeatured = (summarized) => async dispatch => {
  const res = await axios.get('/api/featured',{
      params: {
          summarizedVal: summarized
      }
  });
  dispatch({type:FETCH_FEATURED, payload: res.data });
};

export const fetchCategories = () => async dispatch => {
  const res = await axios.get('/api/categories');
  dispatch({type:FETCH_CATEGORIES, payload: res.data });
};

export const saveCategory = (id) => async dispatch => {
  const res = await axios.post(`/api/categories/${id}`);
  dispatch({ type:SAVE_CATEGORY, payload:res.data });
};

export const removeCategory = (id) => async dispatch => {
  const res = await axios.delete(`/api/categories/${id}`);
  dispatch({ type:REMOVE_CATEGORY, payload:res.data });
};

export const updateReminders = (durationID, truthVal) => async dispatch => {

    if(durationID === 'sevenDays') {
        const res = await axios.post(`/api/profile/reminders/sevenDays/${truthVal}`);
        dispatch({ type:UPDATE_REMINDERS, payload:res.data });
    }
    else if (durationID === 'fiveDays') {
        const res = await axios.post(`/api/profile/reminders/fiveDays/${truthVal}`);
        dispatch({ type:UPDATE_REMINDERS, payload:res.data });
    } else if (durationID === 'threeDays') {
        const res = await axios.post(`/api/profile/reminders/threeDays/${truthVal}`);
        dispatch({ type:UPDATE_REMINDERS, payload:res.data });
    } else {
        const res = await axios.post(`/api/profile/reminders/oneDay/${truthVal}`);
        dispatch({ type:UPDATE_REMINDERS, payload:res.data });
    }
};

export const updateFrequency = (truthVal) => async dispatch => {
    const res = await axios.post(`/api/profile/frequency/${truthVal}`);
    dispatch({ type:UPDATE_FREQUENCY, payload:res.data });
};

export const postUnsub = (unsubReason,trialId) => async dispatch => {
  const res = await axios.post('/api/data/unsub_reasons', {unsubReason, trialId});
  dispatch({ type:POST_UNSUB, pay:res.data });
};

export const fetchSingleCategory = (id) => async dispatch => {
  const res = await axios.get(`/api/categories/${id}`);
    dispatch({ type:FETCH_SINGLE_CATEGORY, payload:res.data});
};
export const fetchTrialsByCategory = (category) => async dispatch => {
    const res = await axios.post('/search-categories', { searchTerm:category });
    dispatch({ type: FETCH_TRIALS_BY_CATEGORY, payload: res.data });
};

export const sendSubscriptionToBackend = (subscription) => async dispatch => {
    const headers = {
        'Content-Type': 'application/json'
    };
    const data = {
        subscriptionObj: JSON.stringify(subscription)
    };
    const res = await axios.post('/api/save-subscription',data, headers );
    dispatch({ type: SAVE_NOTIFICATION_SUBSCRIPTION, payload: res.data });
};

export const fetchSelectedCategories = () => async dispatch => {
    const res = await axios.get('/api/my-categories');
    dispatch({type:FETCH_SELECTED_CATEGORIES, payload: res.data });
};

export const fetchNotificationState = () => async dispatch => {
  const res = await axios.get('/api/notification-state');
  dispatch({ type:FETCH_NOTIFICATION_STATE, payload: res.data });
};


export const fetchOpeningQuestions = () => async dispatch => {
    const res = await axios.get('/api/intro/prompts');
    dispatch({ type:RETRIEVE_QUESTIONS, payload:res.data });
};

export const submitResults = (values, history) => async dispatch => {
    const res = await axios.post('/api/intro/survey', values);
    dispatch({type: SUBMIT_SURVEY, payload:res.data});
};

export const fetchCustom = () => async dispatch => {
    const res = await axios.get('/api/custom/categories');
    dispatch({ type:RETRIEVE_CUSTOM, payload:res.data });
};

export const fetchResponses = () => async dispatch => {
  const res = await axios.get('/api/responses/survey');
  dispatch({ type:RETRIEVE_RESPONSES, payload:res.data });
};

export const becomeTester = (trialId) => async dispatch => {
  const res = await axios.get(`/api/tester/${trialId}`);
    dispatch({ type:BECOME_TESTER, payload:res.data });
};

export const findTester = (trialId) => async dispatch => {
  const res = await axios.get(`/api/tester/connect/${trialId}`);
    dispatch({ type:FIND_TESTER, payload:res.data });

};

export const fetchEligibility = () => async dispatch => {
    const res = await axios.get('/api/testing/list');
    dispatch({ type:CHECK_ELIGIBILITY, payload:res.data });

};

export const connectRequest = (trialId, testerId, optionsAvailable) => async dispatch => {
    const res = await axios.post('/api/tester/match', { trialId,testerId, optionsAvailable });
    dispatch({type: CONNECT_REQUEST, payload:res.data});
};

export const connectReject = (trialId, testerId) => async dispatch => {
    const res = await axios.post('/api/tester/reject-match', { trialId,testerId });
    dispatch({type: REJECT_MATCH, payload:res.data});
};

export const confirmTime = (trialId, testerId, userId, optionNumber) => async dispatch => {
    const res = await axios.post('/api/tester/confirm/match', { trialId,testerId, userId, optionNumber});
    dispatch({type: CONFIRM_TIME, payload:res.data});

};

export const fetchMatches = () => async dispatch => {
    const res = await axios.get('/api/match/list');
    dispatch({ type:FETCH_MATCHES, payload:res.data });

};
export const postClicks = (trialId) => async dispatch => {
  const res = await axios.post('/api/data/click-record',{ trialId });
  dispatch({ type:DATA_CLICKS, payload:res.data });
};
