/**
 * Created by AnthonyMaina on 10/31/17.
 */

import { FETCH_ACTIVE } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_ACTIVE:
            return action.payload;
        default:
            return state;
    }
}