/**
 * Created by AnthonyMaina on 11/7/17.
 */

import { FETCH_FEATURED } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_FEATURED:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}