/**
 * Created by AnthonyMaina on 2/13/18.
 */

import { RETRIEVE_CUSTOM } from '../actions/types';

export default function(state = [],action) {

    switch(action.type) {
        case RETRIEVE_CUSTOM:
            return action.payload;
        default:
            return state;
    }
};