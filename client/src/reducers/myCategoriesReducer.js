/**
 * Created by AnthonyMaina on 12/11/17.
 */

import { FETCH_SELECTED_CATEGORIES } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_SELECTED_CATEGORIES:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}