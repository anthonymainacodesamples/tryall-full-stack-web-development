/**
 * Created by AnthonyMaina on 10/19/17.
 */
import { FETCH_TRIAL } from '../actions/types';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_TRIAL:
            return action.payload[0];
        default:
            return state;
    }
}