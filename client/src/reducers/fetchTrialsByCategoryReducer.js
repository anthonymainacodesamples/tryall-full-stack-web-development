/**
 * Created by AnthonyMaina on 12/6/17.
 */

import { FETCH_TRIALS_BY_CATEGORY } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case FETCH_TRIALS_BY_CATEGORY:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}