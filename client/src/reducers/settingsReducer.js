/**
 * Created by AnthonyMaina on 10/24/17.
 */

import { FETCH_SETTINGS } from '../actions/types';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_SETTINGS:
            return action.payload[0];
        default:
            return state;
    }
}