/**
 * Created by AnthonyMaina on 2/15/18.
 */


import { RETRIEVE_RESPONSES} from '../actions/types';

export default function(state = {},action) {

    switch(action.type) {
        case RETRIEVE_RESPONSES:
            return action.payload;
        default:
            return state;
    }
};