/**
 * Created by AnthonyMaina on 12/6/17.
 */


import { FETCH_SINGLE_CATEGORY } from '../actions/types';

export default function(state = {}, action) {


    switch (action.type) {
        case FETCH_SINGLE_CATEGORY:
            return action.payload[0];
        default:
            return state;
    }
}