/**
 * Created by AnthonyMaina on 2/11/18.
 */

import { RETRIEVE_QUESTIONS } from '../actions/types';

export default function(state = [],action) {

    switch(action.type) {
        case RETRIEVE_QUESTIONS:
            return action.payload;
        default:
            return state;
    }
};