/**
 * Created by AnthonyMaina on 10/8/17.
 */
import { FETCH_RESULTS } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case FETCH_RESULTS:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}