/**
 * Created by AnthonyMaina on 10/23/17.
 */

import { FETCH_SAVED } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_SAVED:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}