/**
 * Created by AnthonyMaina on 3/6/18.
 */


import { CHECK_ELIGIBILITY } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case CHECK_ELIGIBILITY:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}