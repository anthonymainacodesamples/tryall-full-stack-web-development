/**
 * Created by AnthonyMaina on 12/18/17.
 */


import { FETCH_NOTIFICATION_STATE } from '../actions/types';

export default function(state = null, action) {

    switch (action.type) {
        case FETCH_NOTIFICATION_STATE:
            return action.payload;
        default:
            return state;
    }
}