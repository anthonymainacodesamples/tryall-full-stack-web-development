/**
 * Created by AnthonyMaina on 10/5/17.
 */
import { combineReducers } from 'redux';
import { reducer as reduxForm } from 'redux-form';
import authReducer from './authReducer';
import searchReducer from './searchReducer';
import recommendedReducer from './recommendedReducer';
import singleTrialReducer from './singleTrialReducer';
import savedTrialsReducer from './savedReducer';
import settingsReducer from './settingsReducer';
import similarReducer from './similarTrialsReducer';
import activeReducer from './activeReducer';
import pastReducer from './pastReducer';
import featuredReducer from './featuredReducer';
import categoriesReducer from './categoriesReducer';
import singleCategoryReducer from './singleCategoryReducer';
import trialsByCategoryReducer from './fetchTrialsByCategoryReducer';
import myCategoriesReducer from './myCategoriesReducer';
import notificationStateReducer from './notificationStateReducer';
import introSurveyReducer from './introSurveyReducer';
import personalCategoryReducer from './personalCategoryReducer';
import surveyResponsesReducer from './surveyResponsesReducer';
import singleTesterReducer from './singleTesterReducer';
import matchesListReducer from './matchesListReducer';
import eligibilityReducer from './eligibilityReducer';

export default combineReducers({
    auth: authReducer,
    search:searchReducer,
    recommended:recommendedReducer,
    singleTrial:singleTrialReducer,
    similarTrials:similarReducer,
    settings:settingsReducer,
    savedTrials:savedTrialsReducer,
    activeTrials:activeReducer,
    pastTrials:pastReducer,
    featured:featuredReducer,
    categories: categoriesReducer,
    singleCategory: singleCategoryReducer,
    trialsByCategory:trialsByCategoryReducer,
    myCategories: myCategoriesReducer,
    notificationState: notificationStateReducer,
    introQuestions: introSurveyReducer,
    personalCategory:personalCategoryReducer,
    surveyResponses: surveyResponsesReducer,
    identifiedTesterInfo: singleTesterReducer,
    matchesList:matchesListReducer,
    eligibilityItems:eligibilityReducer,
    form: reduxForm
});