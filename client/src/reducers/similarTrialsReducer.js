/**
 * Created by AnthonyMaina on 10/29/17.
 */

import { FETCH_SIMILAR } from '../actions/types';
import _ from 'lodash';

export default function(state = null, action) {

    switch (action.type) {
        case FETCH_SIMILAR:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}