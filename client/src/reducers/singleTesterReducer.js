/**
 * Created by AnthonyMaina on 2/20/18.
 */

import { FIND_TESTER } from '../actions/types';

export default function(state = {}, action) {

    switch (action.type) {
        case FIND_TESTER:
            return action.payload.connectInfo;
        default:
            return state;
    }
}