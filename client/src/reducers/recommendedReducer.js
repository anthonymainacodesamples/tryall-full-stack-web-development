/**
 * Created by AnthonyMaina on 10/18/17.
 */

import { FETCH_RECOMMENDED } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {

    switch (action.type) {
        case FETCH_RECOMMENDED:
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}