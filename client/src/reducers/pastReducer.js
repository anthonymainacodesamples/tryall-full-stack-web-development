/**
 * Created by AnthonyMaina on 11/1/17.
 */


import {FETCH_PAST } from '../actions/types';
import _ from 'lodash';

export default function(state = {}, action) {
    switch (action.type) {
        case FETCH_PAST :
            return _.mapKeys(action.payload, '_id');
        default:
            return state;
    }
}