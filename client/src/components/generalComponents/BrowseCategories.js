/**
 * Created by AnthonyMaina on 12/6/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { Glyphicon } from 'react-bootstrap';
import { fetchCategories } from '../../actions';
import _ from 'lodash';
import '../../styles/browseCategories.css';


class BrowseCategories extends Component {

    componentWillMount() {
        this.props.fetchCategories();
    }


    renderIndividualCategory() {
        const { categories } = this.props;
        return _.map(categories, item => {
            return(
                <li className="Product" key={item._id}>
                    <Link to={`/category/${item._id}`}  style={{ textDecoration: 'none' }}>
                        <i  className={`Product-icon fa ${item.browse_icon}`}/>
                        <h6 className="Product-name">
                            {item.name}
                        </h6>
                    </Link>
                </li>
            );
        })
    }
    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Categories</h3>
                                <p className="sub-heading"> Browse our extensive catalog using the following categories.</p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    <Link className="home-banner-button" to={`/update/categories`} style={{ textDecoration: 'none' }}>
                                         Update Followed Categories
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderListOfCategories() {
        return (
            <div>
                <ul className="ProductList">
                    {this.renderIndividualCategory()}
                </ul>
            </div>
        );
    }

    renderTitle() {
        return (
                <div className="container">
                <div className="row text-center ">
                    <h3> <Glyphicon glyph="th"/> Browse our extensive selection using the following categories</h3>
                </div>
                </div>
        );
    }

    render() {
        return (
            <div>
                {/*{this.renderTitle()}*/}
                {this.renderTopBanner()}
                {this.renderListOfCategories()}
            </div>
        );
    }
}

function mapStateToProps({ categories }) {
    return ({ categories });
}


export default connect(mapStateToProps, { fetchCategories })(BrowseCategories);