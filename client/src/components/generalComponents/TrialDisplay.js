/**
 * Created by AnthonyMaina on 10/17/17.
 */

//Package components
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Fade, Glyphicon} from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import AlertContainer from 'react-alert';
import { Link } from 'react-router-dom';
import _ from 'lodash';


//Custom components and actions
import { fetchTrial,
    fetchSimilar,
    saveReminder,
    saveTrial,
    saveActiveTrial,
    fetchSaved,
    fetchActive,
    removeFromSaved,
    sendSubscriptionToBackend,
    fetchNotificationState,
    becomeTester, findTester,fetchEligibility} from '../../actions/index';
import '../../styles/trialDisplay.css';
import 'react-datepicker/dist/react-datepicker.css';
import PlaceHolder from '../../images/placehold.png';
import SubscribeModal from '../miniComponents/SubscribeModal';
import SubQuestionModal from '../miniComponents/SubQuestionModal';
import TrialList from './TrialList';
import SuccessIcon from '../../images/success.png';
import WebPushNotification from '../utils/WebPushNotifications';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import history from '../utils/history';




class TrialDisplay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalShow: false,
            questionModalShow:false,
            open:false,
            startDate: moment()
        };


        //Initial Modal
        this.closeModal = this.closeModal.bind(this);
        this.openModal = this.openModal.bind(this);
        this.connectToTester = this.connectToTester.bind(this);

        //Second Modal
        this.closeQuestionModal = this.closeQuestionModal.bind(this);
        this.openQuestionModal = this.openQuestionModal.bind(this);
        this.switchModal =  this.switchModal.bind(this);
        this.addToActive = this.addToActive.bind(this);
        this.didNotSubscribe = this.didNotSubscribe.bind(this);
        this.updateSubscriptionOnServer = this.updateSubscriptionOnServer.bind(this);
        this.registerUserForNotifications = this.registerUserForNotifications.bind(this);
        this.becomeATester = this.becomeATester.bind(this);

        //Date bindings
        this.toggleDate = this.toggleDate.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    alertOptions = {
        offset: 14,
        position: 'top right',
        theme: 'light',
        time: 5000,
        transition: 'scale'
    };

    showAlert = () => {
        this.msg.show('Successfully saved trial.', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };
    showNoTestersAlert = () => {
        this.msg.show('No testers available', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };
    showBecomeTesterAlert = () => {
        this.msg.show('You are now a tester for this product', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    showReminderAlert = () => {
        this.msg.show('Your reminder has been set.', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };
    showRemovalAlert = () => {
        this.msg.show('Trial removed from saved', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    handleChange(date) {
        this.setState({
            startDate: date
        });
    }
    componentDidMount() {
        const { id } = this.props.match.params;
        this.dataFetch(id);
        this.props.fetchNotificationState();
        window.scrollTo(0, 0)

    }

    dataFetch(id) {
        this.props.fetchTrial(id);
        this.props.fetchActive();
        this.props.fetchSimilar(id);
        this.props.fetchEligibility();


    }

    componentWillReceiveProps(newProps) {
        if(this.props.match.params.id !== newProps.match.params.id) {
            this.dataFetch(newProps.match.params.id);
        }
    }

    switchModal() {
        const { id } = this.props.match.params;
        this.closeModal();
        history.push(`/user/confirm/${id}`);
    }
    closeModal() {
        this.setState({modalShow:false});
    }
    openModal() {
        this.setState({modalShow:true});
    }
    closeQuestionModal() {
        this.setState({questionModalShow:false});
    }
    openQuestionModal() {
        this.setState({questionModalShow:true});
    }

    toggleDate() {
        this.setState({open: !this.state.open});

    }
    renderSimilar() {
        const { similarTrials } = this.props;
        return (
            <TrialList itemList={similarTrials}/>
        );
    }
    renderReminderInfo() {
        return (
            <div className="text-center date-section">
                <p>
                    You will be reminded 3 days before your trial ends. <br />
                    <Link to={`/settings`}  style={{ textDecoration: 'none' }}>
                        Click here to change this.
                    </Link>
                </p>
            </div>
        );
    }
    saveReminder({ _id }) {
        this.props.saveReminder(_id, this.state.startDate);
        this.props.saveActiveTrial(_id);
        this.showReminderAlert();
        const notifState = this.props.notificationState;
        if(!notifState.webPushSubscription) {
            this.registerUserForNotifications();
            this.toggleDate();
        } else {
            console.log('Will not prompt');
            this.toggleDate();
        }


    }

    removeTrialFromSaved(id) {
        this.props.removeFromSaved(id);
        this.showRemovalAlert()
    }

    saveTrialForLater(id) {
        this.props.saveTrial(id);
        this.showAlert();
    }


    registerUserForNotifications() {
        //Required Keys
        const applicationServerPublicKey = WebPushNotification.applicationServerPublicKey;
        const applicationServerKey = WebPushNotification.urlBase64ToUint8Array(applicationServerPublicKey);

        //General Constants
        let isSubscribed = false;
        let swRegistration = null;
        let currentObject = this;

        if ('serviceWorker' in navigator && 'PushManager' in window) {
            console.log('Service Worker and Push is supported');

            navigator.serviceWorker.register(`${process.env.PUBLIC_URL}/sw.js`)
                .then(function(swReg) {
                    console.log('Service Worker is registered', swReg);

                    swRegistration = swReg;
                    swRegistration.pushManager.subscribe({
                        userVisibleOnly: true,
                        applicationServerKey: applicationServerKey
                    })
                        .then(function(subscription) {
                            console.log('User is subscribed.');

                            currentObject.updateSubscriptionOnServer(subscription);

                            isSubscribed = true;

                        })
                        .catch(function(err) {
                            console.log('Failed to subscribe the user: ', err);
                        });

                })
                .catch(function(error) {
                    console.error('Service Worker Error', error);
                });
        } else {
            console.warn('Push messaging is not supported');
        }
    }

    updateSubscriptionOnServer(subscription) {
        this.props.sendSubscriptionToBackend(subscription);
    }



    addToActive(currentTrial) {
        this.props.saveReminder(currentTrial._id, this.state.startDate);
        this.props.saveActiveTrial(currentTrial._id,this.state.startDate,currentTrial.DurationInDays);
        this.showReminderAlert();
        this.closeQuestionModal()
    }

    didNotSubscribe() {
        this.closeQuestionModal()
    }

    renderSavedButton(id) {
        const { savedTrials } = this.props;

        if(savedTrials[id]) {
            return (
                <button
                    onClick={() => this.removeTrialFromSaved(id)}
                    className="home-banner-button">

                    <i className="fa fa-times"/> Remove from Saved
                </button>
            );
        } else {
            return (
                <button
                    onClick={() => this.saveTrialForLater(id)}
                    className="home-banner-button">

                    <i className="fa fa-bookmark-o fa-fw"/> Save For Later
                </button>
            );
        }
    }


    renderTesterButton(id) {
        const { activeTrials,activeTesting,pendingTrials ,eligibilityItems} = this.props;


        const tooltip = (
            <Tooltip id="tooltip">
                 Speak to someone also testing out this product.
            </Tooltip>
        );
        const tooltipBecome = (
            <Tooltip id="tooltip">
                People can message you about your experience.
            </Tooltip>
        );

        const tooltipNotAllowed = (
            <Tooltip id="tooltip">
                You are already a tester.
            </Tooltip>
        );

        const tooltipPending = (
            <Tooltip id="tooltip">
                Already matched and pending response.
            </Tooltip>
        );


        if(_.some(activeTrials, {_id:id})) {
            // if(!(_.includes(activeTesting, id))) {
            if(!(eligibilityItems[id])) {
                return (
                    <Link to={`/tester/subscribe/${id}`}>
                        <OverlayTrigger placement="top" overlay={tooltipBecome}>
                            <button className="dark-blue-general-button pull-right">
                                <i className="fa fa-comments"/> Become a Tester
                            </button>
                        </OverlayTrigger>
                    </Link>
                );
            } else {
                return (
                    <OverlayTrigger placement="top" overlay={tooltipNotAllowed}>
                        <button className="dark-blue-general-button pull-right disabled-tester">
                            <i className="fa fa-comments"/> Become a Tester
                        </button>
                    </OverlayTrigger>
                );
            }
        } else {
            if(!(_.includes(pendingTrials, id))) {
                return (
                    <Link to={`/match/${id}`}>
                        <OverlayTrigger placement="top" overlay={tooltip}>
                            <button className="dark-blue-general-button pull-right"
                                    onClick={() => this.props.findTester(id)}
                            >
                                <i className="fa fa-comments"/>
                                {' '}Speak To a Tester
                            </button>
                        </OverlayTrigger>
                    </Link>
                );
            } else {
                return (
                        <OverlayTrigger placement="top" overlay={tooltipPending}>
                            <button className="dark-blue-general-button pull-right disabled-tester"
                            >
                                <i className="fa fa-comments"/>
                                {' '}Speak To a Tester
                            </button>
                        </OverlayTrigger>
                );
            }
        }
    }

    becomeATester(id) {
        this.props.becomeTester(id);
        this.showBecomeTesterAlert()
    }

    connectToTester() {
        const { identifiedTesterInfo } = this.props;


            const {singleTester, currentUserInfo} = identifiedTesterInfo;

        if (singleTester) {
            //Connect with SingleTester
            let testerId = singleTester.userId;
            let testerName = singleTester.userName;


            // CurrentUserInfo is for logged in User
            let currentUserId = currentUserInfo.currentId;
            let currentName = currentUserInfo.currentName;


            // let appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
            // // For typical 1-to-1 chat which is unique between two users
            // if(window.sbWidget) {
            // let userIds = [userId, 'nealcaffrey'];
            //     window.sbWidget.launchTesterChat(userIds);
            // }


            let appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
            let userId = currentUserId;
            let nickname = currentName;
            window.sbWidget.startWithConnect(appId, userId, nickname, function () {
                // do something...
                let userIds = [userId, testerId];
                window.sbWidget.launchTesterChat(userIds);
            });
//
//             //        var appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
// //        var userId = 'maina';
// //        var nickname = 'Anthony Maina';
// //        window.sbWidget.startWithConnect(appId, userId, nickname, function() {
// //          // do something...
// //            console.log('Done');
// ////            window.sbWidget
//        });
        } else {
            this.showNoTestersAlert()
        }
    }
    renderCorrectSubscribeButton(id, siteLink) {
        const { activeTrials } = this.props;

        if(_.some(activeTrials, {_id:id})) {
            return (
                <a href={siteLink} target="_blank">
                <button className="dark-blue-general-button large-trial-display"
                >
                    <Glyphicon glyph="link"/> Go To Site
                </button>
                </a>
            );
        } else {
            return (
                <button className="dark-blue-general-button large-trial-display"
                        onClick={this.openModal}
                >
                    <Glyphicon glyph="link"/> Go To Site and Subscribe
                </button>
            );
        }
    }

    renderAlreadySubscribed(id) {
        const { activeTrials } = this.props;

        const tooltip = (
            <Tooltip id="tooltip">
                You are already subscribed.
            </Tooltip>
        );

        if(_.some(activeTrials, {_id:id})) {
            return (
                <OverlayTrigger placement="bottom" overlay={tooltip}>
                <button
                    className="dark-blue-general-button pull-right disabled-tester"
                >
                    Already Subscribed?
                </button>
                </OverlayTrigger>
            );
        } else {
            return (
                <button
                    className="dark-blue-general-button pull-right"
                    onClick={this.toggleDate}
                >
                    Already Subscribed?
                </button>
            );

        }

    }

    renderContent(currentTrial) {
        return (
            <div className="row text-center ">
                <div className="col-md-7">
                    <div className="display-sizing">
                        <img className="display-trial-image" src={currentTrial.logoLink ? currentTrial.logoLink : PlaceHolder} alt="" />
                    </div>
                    <p>{currentTrial.description? currentTrial.description : 'No description available yet'}</p>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                    { this.renderCorrectSubscribeButton(currentTrial._id, currentTrial.websiteLink)}
                </div>

                <div className="action-buttons col-md-5">
                    {this.renderTesterButton(currentTrial._id) }
                    {this.renderAlreadySubscribed(currentTrial._id)}

                </div>
                <div className="col-md-5">
                    <Fade in={this.state.open}>
                        <div className="text-center">
                            <h4>When did you subscribe?</h4>
                            <div className="row">
                                <div className="col-xs-6 col-md-6 ">
                                    <DatePicker
                                        selected={this.state.startDate}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="col-xs-6 col-md-6">
                                    <button
                                        onClick={() => this.saveReminder(currentTrial)}
                                        className="small-connect-button"
                                    >Submit Date</button>
                                </div>
                            </div>
                            {this.renderReminderInfo()}
                        </div>
                    </Fade>
                </div>
                <SubscribeModal
                    show={this.state.modalShow}
                    onHide={this.closeModal}
                    trialLink={currentTrial.websiteLink}
                    switchModal = {this.switchModal}
                    title={currentTrial.name}
                />
                <SubQuestionModal
                    show={this.state.questionModalShow}
                    onHide={this.closeQuestionModal}
                    addToActive = {() => this.addToActive(currentTrial)}
                    trialID={currentTrial._id}
                    clickedDidNotSubscribe = {this.didNotSubscribe}
                    title={currentTrial.name}
                />
            </div>
        );
    }

    renderHeader() {
        const { currentTrial } = this.props;
        return (
            <div className="page-header-tryall">
                <p> Trials Similar to {currentTrial.name} </p>
            </div>
        );
    }
    renderTopBanner(currentTrial) {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> {currentTrial.name}</h3>
                                <p className="sub-heading"> {currentTrial.tagLine}</p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    {this.renderSavedButton(currentTrial._id)}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { currentTrial } = this.props;

        if(!currentTrial) {
            return <div>Loading Trial...</div>;
        }
        return(
            <div>
                {this.renderTopBanner(currentTrial)}
                <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                <div className="container">
                    {this.renderContent(currentTrial)}
                    {this.renderHeader()}
                    {this.renderSimilar()}
                </div>
            </div>
        );
    }
}

function mapStateToProps({ singleTrial, similarTrials, savedTrials, activeTrials, notificationState, identifiedTesterInfo,eligibilityItems }) {
    return {
        currentTrial: singleTrial,
        similarTrials,
        savedTrials,
        activeTrials:activeTrials.trials,
        activeTesting:activeTrials.trialsTesterIds,
        pendingTrials:activeTrials.pendingTrialsIds,
        notificationState,
        eligibilityItems,
        identifiedTesterInfo

    };
}

export default connect(mapStateToProps, {
    fetchTrial,
    fetchSimilar,
    saveReminder,
    saveTrial,
    saveActiveTrial,
    fetchSaved,
    fetchActive,
    removeFromSaved,
    sendSubscriptionToBackend,
    fetchNotificationState,
    becomeTester,
    findTester,fetchEligibility })(TrialDisplay);


