/**
 * Created by AnthonyMaina on 12/3/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PlaceHolder from '../../images/placehold.png';
import { Link} from 'react-router-dom';
import NotSubCheckBox from '../miniComponents/NotSubCheckBox';
import '../../styles/NotSubPage.css';

//Tryall Specific
import { fetchTrial, postUnsub } from '../../actions/index';

const reasonsNotSub = [
    'Price Changed',
    'Inaccurate Information',
    'Link Is Broken',
    'Too Expensive'
];


class FailureToSubscribe extends Component {

    constructor(props) {
        super(props);
        this.state = {term:''};
        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(event) {
        this.setState({term: event.target.value});
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetchTrial(id);
        this.selectedCheckboxes = new Set();
    }

    toggleCheckbox = label => {
        if (this.selectedCheckboxes.has(label)) {
            this.selectedCheckboxes.delete(label);
        } else {
            this.selectedCheckboxes.add(label);
        }
    };

    handleFormSubmit = formSubmitEvent => {
        formSubmitEvent.preventDefault();
        const { trialId } = this.props.match.params;

        for (const checkbox of this.selectedCheckboxes) {
            console.log(checkbox, 'is selected.');
            this.props.postUnsub(checkbox, trialId);
        }

        if(this.state.term) {
            const unsubReason = this.state.term;
            this.props.postUnsub(unsubReason, trialId);
            this.setState({ term:'' });
        }
    };
    createCheckbox = label => (
        <div className="col-md-6"  key={label}>
            <NotSubCheckBox
                label={label}
                handleCheckboxChange={this.toggleCheckbox}
                key={label}
             />
        </div>
    );

    createCheckboxes = () => (
        reasonsNotSub.map(this.createCheckbox)
    );


    componentWillReceiveProps(newProps) {
        if(this.props.match.params.id !== newProps.match.params.id) {
            this.props.fetchTrial(newProps.match.params.id);
        }
    }

    renderFormUnsubReasons() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                {this.createCheckboxes()}
                <div className="row text-center" >
                    <div className="free-input col-md-8 col-md-offset-2">
                        <label>
                            Other Reason:{' '}
                            <input
                            type="text"
                            name="other"
                            value = {this.state.term}
                            onChange={this.onInputChange}
                        />
                        </label>
                    </div>
                </div>
                <Link to={`/homefeed`}  style={{ textDecoration: 'none' }}>
                    <button className="dark-blue-general-button" type="submit">
                        <i className="fa fa-arrow-right fa-fw"/>
                        Submit
                    </button>
                </Link>
        </form>
        );
    }

    renderContent() {
        const { currentTrial } = this.props;
        return(
            <div>
            <div className="row text-center">
                <div className="col-md-3 col-md-offset-2">
                    <div className="display-sizing">
                        <img className="display-trial-image-failure" src={currentTrial.logoLink ? currentTrial.logoLink : PlaceHolder} alt="" />
                    </div>
                </div>
                <div className="col-md-5">
                    <h3>{currentTrial.name} : {currentTrial.tagLine}</h3>
                    <p>{currentTrial.description? currentTrial.description : 'No description available yet'}</p>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                </div>
            </div>
                <div className="row text-center" >
                    <div className="reasons-form col-md-8 col-md-offset-2">
                    {this.renderFormUnsubReasons()}
                    </div>
                </div>
            </div>
        );
    }
    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Reason for not subscribing?</h3>
                                <p className="sub-heading"> Tell us what you didn't like about the trial.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return(
          <div id="wrap">

              {this.renderTopBanner()}
              <div id="main" className="container">
              {this.renderContent()}
              </div>
          </div>
        );
    }
}


function mapStateToProps({ singleTrial }) {
    return {
        currentTrial: singleTrial,
    };
}

export default connect(mapStateToProps, { fetchTrial, postUnsub })(FailureToSubscribe);