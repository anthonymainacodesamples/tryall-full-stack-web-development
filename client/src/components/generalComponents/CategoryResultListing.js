/**
 * Created by AnthonyMaina on 12/6/17.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchTrialsByCategory } from '../../actions/index';
import TrialList from './TrialList';



class CategoryResultListing extends Component {

    renderListResults() {

        const { trialsByCategory } = this.props;

        return (
            <TrialList itemList={trialsByCategory}/>
        );
    }

    render() {
        return (
            <div>
                {this.renderListResults()}
            </div>
        );
    }

}

function mapStateToProps({  trialsByCategory }) {
    return ({ trialsByCategory});
}


export default connect(mapStateToProps, { fetchTrialsByCategory })(CategoryResultListing);