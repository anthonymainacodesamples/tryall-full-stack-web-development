/**
 * Created by AnthonyMaina on 12/11/17.
 */

import React, { Component } from 'react';
import { Glyphicon } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { fetchCategories, fetchSelectedCategories } from '../../actions';

import _ from 'lodash';
//Import SingleCategory
import SingleCategory from './SingleCategory';

class ReselectCategories extends Component {

    componentDidMount() {
        this.props.fetchCategories();
        // this.props.fetchSelectedCategories();
        window.scrollTo(0, 0)

    }

    renderTitle() {
        return (
            <div className="container">
                <div className="row text-center ">
                    <h3> <Glyphicon glyph="check"/> Select categories you are interested in.</h3>
                    <p>These are used to inform our recommendations to you. Pick at least 5 for the best results.</p>

                </div>
            </div>
        );
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Select categories you are interested in.</h3>
                                <p className="sub-heading"> These are used to inform our recommendations to you. Pick at least 5 for the best results.</p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    <Link className="home-banner-button" to={`/homefeed`} style={{ textDecoration: 'none' }}>
                                        Done
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderCategories() {
        const { categories } = this.props;
        const { myCategories } = this.props;

        if(myCategories) {
            return _.map(categories, item => {
                if(myCategories[item._id]) {
                    return (
                        <SingleCategory key={item._id} item={item} isSelected={true}/>
                    );
                } else {
                    return (
                    <SingleCategory key={item._id}  item={item} isSelected={false} />
                    );
                }
            })
        }
    }


    render() {
        return (
            <div>
                {this.renderTopBanner()}
                <ul className="ProductList">
                    {this.renderCategories()}
                </ul>
                {/*<div className="row text-center ">*/}
                    {/*<Link to={`/homefeed`}  style={{ textDecoration: 'none' }}>*/}
                        {/*<button*/}
                            {/*className="gotosite-button Fixed-Bottom-Button"*/}
                        {/*>*/}
                            {/*<Glyphicon glyph="arrow-left"/> Back To Site*/}
                        {/*</button>*/}
                    {/*</Link>*/}
                {/*</div>*/}
            </div>
        );
    }
}
function mapStateToProps({ categories, myCategories }) {
    return ({ categories, myCategories });
}

export default connect(mapStateToProps, { fetchCategories, fetchSelectedCategories })(ReselectCategories);