/**
 * Created by AnthonyMaina on 2/27/18.
 */


import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { connectRequest,connectReject } from '../../actions/index';
import history from '../utils/history';


class TesterMatch extends Component {

    constructor(props) {
        super(props);
        this.handleReject = this.handleReject.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }


    renderTopBanner() {

            return (
                <div className="search-banner">
                    <div className="search-top">
                        <div className="container">
                            <div className="main-search-wrapper">
                                <div className="sub-search-text-div">
                                    <h3 className="heading"> Find a Tester.</h3>
                                    <p className="sub-heading"> You will be able to ask any question as you are deciding whether to try the product out.</p>
                                </div>
                                {/*<div className="text-right search-cat-div ">*/}
                                    {/*<p>*/}
                                        {/*<button*/}
                                            {/*className="home-banner-button"*/}
                                            {/*onClick={() => this.props.connectRequest('5a2a5aa981eb3e7c08b0fda9', '5a2a5aa981eb3e7c08b0fda9')}*/}
                                        {/*>*/}
                                            {/*I Accept This Match*/}
                                        {/*</button>*/}
                                    {/*</p>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
            );
    }
    renderTrialAndMatchInfo(currentTrial) {
        return (
            <div className="row text-center ">
                <div className="col-md-8 col-md-offset-2">
                    <div className="display-sizing">
                        <img className="display-trial-image" src={currentTrial.logoLink} alt="" />
                    </div>
                    <p>{currentTrial.description ? currentTrial.description : 'No description available yet'}</p>

                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                </div>
                </div>
        );
    }

    handleReject(trialId, testerId) {
        this.props.connectReject(trialId,testerId);
        history.push(`/trials/${trialId}`)
    }

    renderContent() {

    const { identifiedTesterInfo } = this.props;
    const { currentTrial } = this.props;
    const {singleTester} = identifiedTesterInfo;


    if(!singleTester || !currentTrial) {
        return (
            <div className="text-center">
                No tester available.
                <div className="text-center consent-content">
                    <Link to={`/trials/${this.props.match.params.id}`}>
                    <button
                        className="dark-blue-general-button invert-general large-trial-display"
                    >
                        Click here to go back.
                    </button>
                    </Link>
                </div>
            </div>
        );
    } else {
        return (
            <div>
                {this.renderTrialAndMatchInfo(currentTrial)}

                <div className="row">
                    <div className=" top-dash-line"/>
                    <div className="consent-content">
                        <p className="main-text text-center col-md-8">
                            <h4>You have been matched with {singleTester.userName}. </h4>
                            * To accept this match tap the following button and you will be connected to each other and you can ask them about their experience with testing out {currentTrial.name}.
                        </p>

                    </div>
                    <div className="text-center">
                        <Link to={`/user/schedule/${currentTrial._id}/${singleTester.userId}`} >
                        <button
                            className="dark-blue-general-button"
                        >
                            I accept this match
                        </button>
                        </Link>
                    </div>
                </div>
                <div className=" top-dash-line"/>
                <div className="text-center consent-content">
                    <button
                        className="dark-blue-general-button invert-general large-trial-display"
                        onClick={() => this.handleReject(currentTrial._id,singleTester.userId)}
                    >
                        I reject this match.
                    </button>
                </div>

            </div>
        );
    }
    }



    render() {
        return (
          <div id="wrap">
              {this.renderTopBanner()}
              <div id="main" className="container">
                  {this.renderContent()}
              </div>
          </div>
        );
    }
}

function mapStateToProps({ identifiedTesterInfo,singleTrial  }) {
    return (
        {
            identifiedTesterInfo,
            currentTrial: singleTrial
        }
        );
}

export default connect(mapStateToProps, { connectRequest, connectReject })(TesterMatch);