/**
 * Created by AnthonyMaina on 10/5/17.
 */

import React from 'react';
import '../../styles/landingPage.css';
import devicesImg from '../../images/section-devices.png';
import appLeImg from '../../images/Download_on_the_App_Store_Badge_US-UK_135x40.svg';
import { Popover, OverlayTrigger } from 'react-bootstrap';

const Landing = () => {

    const popoverHoverFocus = (
        <Popover id="popover-trigger-hover-focus">
            Our iOS app is coming soon.
        </Popover>
    );
    return (
        <div>
            <section className="w-hero">
                <div className="container w-hero-container">
                    <div className="row">
                        <div className="col-sm-8">
                            <h1 className="k-header--x-large k-color--marine">Find the best SaaS tools.</h1>
                            <p className="k-header k-color--night k-spacing-bottom--x-large">
                                Make informed decisions and only pay for tools that work for your business.
                            </p>

                            <a href="/auth/google">
                            <div className="g-sign-in-button">
                                <div className="content-wrapper">
                                    <div className="logo-wrapper">
                                        <img src="https://developers.google.com/identity/images/g-logo.png" alt="google sign in button"/>
                                    </div>
                                    <span className="text-container">
                                        <span>Sign in with Google</span>
                                        </span>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section className="w-showcase">
                <div className="container-fluid w-showcase-container">
                    <div className="row">
                        <div className="w-showcase-left">
                            <img className="w-showcase-karma" src={devicesImg} alt=""/>
                        </div>
                        <div className="w-showcase-right">
                            <div className="w-showcase-contents">
                                <h2 className="k-header--large">Welcome To Tryall.</h2>
                                <p className="k-paragraph">Tryall is a website and mobile app that allows you to test out various subscription based products in a timely and affordable manner.</p>
                                {/*<a class="k-button&#45;&#45;white k-color&#45;&#45;blue" href="https://tryall.co/how-it-works">How It Works</a>*/}
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            <section className="w-under-the-hood k-spacing-padding-top--huge k-spacing-bottom--huge">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 col-md-offset-3">
                            <h2 className="k-header--large k-color--marine k-spacing-bottom--huge k-block">How do we do it?</h2></div>
                    </div>
                    <div className="row w-under-the-hood-row">
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--manage"/>
                            <h3 className="k-header">Smooth Management</h3>
                            <p className="k-paragraph ">Tryall provides a simple and intuitive interface for you to browse and discover new products and keep track of all your subscriptions.</p>
                        </div>
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--recommend"/>
                            <h3 className="k-header">Personalized Recommendations</h3>
                            <p className="k-paragraph">As you keep using Tryall, we better learn what sort of products interest you thus providing you more valuable recommendations.</p>
                        </div>
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--reminder"/>
                            <h3 className="k-header">Safe Testing</h3>
                            <p className="k-paragraph">By allowing you to set custom reminders for your active free trials, you will always be notified right before your free trial ends to avoid unexpected charges.</p>
                        </div>
                    </div>
                </div>
            </section>
            <section className="k-spacing-bottom--huge">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <OverlayTrigger trigger={['hover', 'focus']} placement="top" overlay={popoverHoverFocus}>

                            <img className="w-showcase-badges" src={appLeImg} alt=""/>
                            </OverlayTrigger>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default Landing;