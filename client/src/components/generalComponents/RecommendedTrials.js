/**
 * Created by AnthonyMaina on 10/27/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRecommended } from '../../actions/index';
import { Link } from 'react-router-dom';
import TrialList from './TrialList';

class RecommendedTrials extends Component {
    componentDidMount() {
        this.props.fetchRecommended(false);
        window.scrollTo(0, 0)
    }
    renderRecommended() {
        const itemList = this.props.recommended;
        return (
            <TrialList itemList={ itemList } />
        );
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Based on your preferences</h3>
                                <p className="sub-heading"> Selection of free trials determined by your followed categories.</p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    <Link className="home-banner-button" to={`/update/categories`} style={{ textDecoration: 'none' }}>
                                        Update Followed Categories
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.renderTopBanner()}
                <div className="container">
                    { this.renderRecommended()}
                </div>
            </div>
        );
    }
}
function mapStateToProps({ recommended }) {
    return ({ recommended });
}

export default connect(mapStateToProps, { fetchRecommended })(RecommendedTrials);