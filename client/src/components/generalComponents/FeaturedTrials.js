/**
 * Created by AnthonyMaina on 11/7/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFeatured } from '../../actions/index';
import TrialList from './TrialList';


class FeaturedTrials extends Component {

    componentDidMount() {
        this.props.fetchFeatured(false);
        window.scrollTo(0, 0)
    }
    renderFeatured() {
        const itemList = this.props.featured;
        return (
            <TrialList itemList={ itemList } />
        );
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Featured Free Trials</h3>
                                <p className="sub-heading"> Handpicked selection of the most popular trials right now.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    render() {
        return (
            <div>
                {this.renderTopBanner()}
                <div className="container">
                { this.renderFeatured()}
                </div>
            </div>
        );
    }
}

function mapStateToProps({ featured }) {
    return ({ featured });
}
export default connect(mapStateToProps, { fetchFeatured })(FeaturedTrials);