/**
 * Created by AnthonyMaina on 11/29/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { fetchCategories } from '../../actions';

import _ from 'lodash';

//Import SingleCategory
import SingleCategory from './SingleCategory';

class CategoriesSelection extends Component {

    componentDidMount() {
        this.props.fetchCategories();
        window.scrollTo(0, 0)
    }


    renderCategories() {
        const { categories } = this.props;

        return _.map(categories, item => {
            return(
                    <SingleCategory key={item._id}  item={item}/>
            );
        })
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Pick Categories you're interested in</h3>
                                <p className="sub-heading"> These will be used to inform our recommendations to you. Pick at least 5 for the best results.</p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    <Link className="home-banner-button" to={`/homefeed`} style={{ textDecoration: 'none' }}>
                                        Proceed To Site
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }



    render() {
        return (
            <div id="wrap">
                {this.renderTopBanner()}
                <div id="main">
                <ul className="ProductList">
                    {this.renderCategories()}
                </ul>
                <div className="row text-center ">
                    <Link to={`/homefeed`}  style={{ textDecoration: 'none' }}>
                    </Link>
                </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps({ categories }) {
    return ({ categories });
}

export default connect(mapStateToProps, { fetchCategories }) (CategoriesSelection);