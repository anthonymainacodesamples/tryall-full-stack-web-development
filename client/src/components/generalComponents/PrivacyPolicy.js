/**
 * Created by AnthonyMaina on 2/10/18.
 */

import React, { Component } from 'react';

class PrivacyPolicy extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }


    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Privacy Policy</h3>
                                <p className="sub-heading"> The complete Privacy Policy between Tryall and all its users.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return(
            <div id="wrap">
                {this.renderTopBanner() }
                <div id="main">

                </div>

            </div>
        );
    }
}

export default PrivacyPolicy;