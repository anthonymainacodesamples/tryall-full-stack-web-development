/**
 * Created by AnthonyMaina on 2/10/18.
 */

import React, { Component } from 'react';

class TermsAndConditions extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Terms and Conditions</h3>
                                <p className="sub-heading"> The complete terms and cnditions pertaining to usage of Tryall.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render(){
        return (
            <div id="wrap">
                {this.renderTopBanner()}
                <div id="main">

                </div>
            </div>
        );
    }
}
export default TermsAndConditions;