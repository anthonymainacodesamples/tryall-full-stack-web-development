/**
 * Created by AnthonyMaina on 3/4/18.
 */


import React, { Component } from 'react';
import * as Vibrant from 'node-vibrant'
import moment from 'moment';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

class MatchItem extends Component {


    constructor(props) {
        super(props);
        this.state = {
            testerTag:false,
            color: [50, 100, 150]
        };
    }

    formatColor(ary) {
        return 'rgb(' + ary.join(', ') + ')';
    }


    componentDidMount() {

        const { item,testerStatus } = this.props;
        this.setState({
           testerTag:testerStatus
        });

        let v = new Vibrant(`${item.logoLink}`, {
            'Access-Control-Allow-Origin': 'http://localhost:3000',
            'Access-Control-Allow-Credentials': 'true'
        });
        v.getPalette((err, palette) => {
            if(palette && palette.Vibrant) {
                // console.log("Works for ", item.name);
                this.setState({
                    color: palette.Vibrant._rgb
                });
            } else {
                // console.log("Doesn't work for ", item.name);
            }
        });
    }
    renderBadge() {
        if(this.state.testerTag) {
            return (
                <div className="match-badge match-badge-danger match-badge-outlined">TESTER</div>

            );
        } else {
            return (
                <div className="match-badge match-badge-success match-badge-outlined">NOT A TESTER</div>
            );

        }
    }

    renderConnectButton() {
        const { connectTime} = this.props;

        const tooltip = (
            <Tooltip id="tooltip">
                Not yet time.
            </Tooltip>
        );

        if(moment().isBefore(moment(connectTime))) {
            return (
                <div className="row text-center">
                    <OverlayTrigger placement="bottom" overlay={tooltip}>

                    <button
                        className="small-connect-button small-connect-disabled"
                    >
                        Click to chat
                    </button>
                    </OverlayTrigger>

                </div>
            );

        } else {
            return (
                <div className="row text-center">
                    <button
                        className="small-connect-button"
                        onClick={() => this.connectToTester()}
                    >
                        Click to chat
                    </button>

                </div>
            );

        }
    }

    connectToTester() {

        const { connectToId, selfId, selfName } = this.props;

        // const { identifiedTesterInfo } = this.props;
        //
        //
        // const {singleTester, currentUserInfo} = identifiedTesterInfo;

            //Connect with SingleTester
            // let testerId = singleTester.userId;
            // let testerName = singleTester.userName;
            //
            //
            // // CurrentUserInfo is for logged in User
            // let currentUserId = currentUserInfo.currentId;
            // let currentName = currentUserInfo.currentName;


            // let appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
            // // For typical 1-to-1 chat which is unique between two users
            // if(window.sbWidget) {
            // let userIds = [userId, 'nealcaffrey'];
            //     window.sbWidget.launchTesterChat(userIds);
            // }


            let appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
            window.sbWidget.startWithConnect(appId, selfId, selfName, function () {
                let userIds = [selfId, connectToId];
                window.sbWidget.launchTesterChat(userIds);
            });
//
//             //        var appId = '487B6D32-9206-4B3D-B817-E0CA7E735206';
// //        var userId = 'maina';
// //        var nickname = 'Anthony Maina';
// //        window.sbWidget.startWithConnect(appId, userId, nickname, function() {
// //          // do something...
// //            console.log('Done');
// ////            window.sbWidget
//        });
    }


    render(){

        const { item, connectToName, connectToId, connectTime, selfId, selfName } = this.props;
        const id = item._id;
        const selectedDate = moment(connectTime).format('LLLL');
        const imageLink = item.logoLink;

        return (
                <div className="thumbnail thumbnail-match" style={{ borderLeftColor:this.formatColor(this.state.color) }}>
                    <img className="group list-group-image" src={imageLink} alt="" />
                    <div className="caption">
                        <h4 className="group inner list-group-item-heading text-center title-space">
                            {item.name}</h4>
                        <div className="text-center">
                            {this.renderBadge()}
                        </div>
                        <p className="group inner list-group-item-text trial-panel text-center">
                            {item.description? item.description : 'No description is available yet'} </p>
                        <div className=" top-dash-line"/>
                        <p className = "list-group-item-text match-panel text-center">
                            Matched with { connectToName } <br/>
                            { selectedDate }
                        </p>
                        {this.renderConnectButton()}
                    </div>
                </div>
        );
    }
}

export default MatchItem;