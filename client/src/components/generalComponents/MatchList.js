/**
 * Created by AnthonyMaina on 3/4/18.
 */


import React, { Component } from 'react';
import MatchItem from './MatchItem';
import _ from 'lodash';
import '../../styles/trialList.css'

class MatchList extends Component {


    //Trials Where Person is Tester
    renderTesterTrials() {
        const { selfInfo,testerTrialsList,testerMatchConnect } = this.props;

        return _.map(testerTrialsList, item => {
            return (
                <div className="item  col-xs-12 col-sm-6 col-md-4" key={item._id}>
                    <MatchItem
                        item={item}
                        selfName = {selfInfo.userName}
                        selfId = {selfInfo.userId}
                        connectToName={testerMatchConnect[item._id].otherPartyName}
                        connectToId={testerMatchConnect[item._id].otherPartyId}
                        connectTime={testerMatchConnect[item._id].timeToSpeak}
                        testerStatus={true}

                    />
                </div>
            );
        });


    }

    //Trials Where Person is User
    renderUserTrials() {
        const { selfInfo,userTrialsList,userMatchConnect } = this.props;

        return _.map(userTrialsList, item => {
            return (
                <div className="item  col-xs-12 col-sm-6 col-md-4" key={item._id}>
                    <MatchItem
                        item={item}
                        selfName = {selfInfo.userName}
                        selfId = {selfInfo.userId}
                        connectToName={userMatchConnect[item._id].otherPartyName}
                        connectToId={userMatchConnect[item._id].otherPartyId}
                        connectTime={userMatchConnect[item._id].timeToSpeak}
                        testerStatus={false}

                    />
                </div>
            );
        });

    }

    render(){
        return(
            <div>
                {this.renderTesterTrials()}
                {this.renderUserTrials()}
            </div>
        );
    }
}
export default MatchList;