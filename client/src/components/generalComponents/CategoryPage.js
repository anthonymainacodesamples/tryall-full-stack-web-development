/**
 * Created by AnthonyMaina on 12/6/17.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSingleCategory, fetchTrialsByCategory } from '../../actions/index';
import CategoryResultListing from './CategoryResultListing';


class CategoryPage extends Component {

    componentDidMount() {
        //Retrieve Category ID
        const { id } = this.props.match.params;
        this.props.fetchSingleCategory(id);
        window.scrollTo(0, 0)


    }

    renderTopBanner() {

        const { name,browse_icon } = this.props.singleCategory;

        if(name) {
            this.props.fetchTrialsByCategory(name);
            return (
                <div>
                <div className="search-banner single-category">
                    <div className="search-top">
                        <div className="container">
                            <div className="main-search-wrapper">
                                <div className="sub-search-text-div">
                                    <h3 className="heading"><i className={`fa ${browse_icon}`}/>{'   '}{ name } Free Trials </h3>
                                    <p className="sub-heading">Click here to suggest a missing product.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div  id="main" className="container">
                    <CategoryResultListing />
                </div>
                </div>
            );
        }
    }
    renderCategoryTitleAndList() {
        const { name,browse_icon } = this.props.singleCategory;

        if(name) {
            this.props.fetchTrialsByCategory(name);
            return (
                <div>
                    <div className="page-header-homepage">
                        <h3><i className={`fa ${browse_icon}`}/>{'   '}{ name } Free Trials </h3>
                    </div>

                </div>
            );
        }
    }

    render() {
        return (
            <div id="wrap">
                {this.renderTopBanner() }
            </div>
        );
    }
}

function mapStateToProps({  singleCategory}) {
    return ({ singleCategory});
}


export default connect(mapStateToProps,{ fetchSingleCategory, fetchTrialsByCategory })(CategoryPage);