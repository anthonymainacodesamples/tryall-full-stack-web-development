/**
 * Created by AnthonyMaina on 2/28/18.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchTrial,saveReminder, saveActiveTrial, becomeTester} from '../../actions/index';
import { Link } from 'react-router-dom';
import SuccessIcon from '../../images/success.png';
import AlertContainer from 'react-alert';
import moment from 'moment';


class ConfirmationSubscription extends Component {

    constructor(props) {
        super(props);
        this.state = {
           currentDate:""
        };

        this.addToActive = this.addToActive.bind(this);

    }
    getCurrentDate() {
        return(
            moment().format("dddd, MMMM Do YYYY")
        );
    }

    addToActive(currentTrial) {
        this.props.saveReminder(currentTrial._id, this.state.currentDate);
        this.props.saveActiveTrial(currentTrial._id,moment(),currentTrial.DurationInDays);
        this.showReminderAlert();
    }

    alertOptions = {
        offset: 14,
        position: 'top right',
        theme: 'light',
        time: 5000,
        transition: 'scale'
    };
    showReminderAlert = () => {
        this.msg.show('Your reminder has been set.', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    componentDidMount() {
        const { id } = this.props.match.params;
        this.dataFetch(id);
        this.setState({
            currentDate:this.getCurrentDate()
        });
        window.scrollTo(0, 0)
    }

    dataFetch(id) {
        this.props.fetchTrial(id);
    }

    becomeATester(id) {
        this.props.becomeTester(id);
        this.showBecomeTesterAlert()
    }



    renderTopBanner({ name }) {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Did you sign up for the {name} free trial on {this.state.currentDate}?</h3>
                                <p className="sub-heading"> Help us provide you with the best possible service by letting us know whether you ended up subscribing.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderButtons() {
        const { currentTrial } = this.props;

        const tooltipBecomeATester = (
            <Tooltip id="tooltip">
                If eligible, people will be able to reach out to you.
            </Tooltip>
        );

        return (
              <div className="row">
                  <div className=" top-dash-line"/>
                  <div className="text-center">
                      <Link to={`/homefeed`}  style={{ textDecoration: 'none' }}>
                      <button
                          className="dark-blue-general-button"
                          onClick={() => this.addToActive(currentTrial)}>
                          Yes, I did.
                      </button>
                      </Link>
                      <Link to={`/tester/subscribe/${currentTrial._id}`}>
                      <button
                          className="dark-blue-general-button invert-general large-trial-display"
                          onClick={() => this.addToActive(currentTrial)}>
                          Yes, become a tester.*
                      </button>
                      </Link>
                      <Link to={`/subfail/${currentTrial._id}`}  style={{ textDecoration: 'none' }}>
                      <button
                          className="dark-blue-general-button"
                      >
                          No, I didn't.
                      </button>
                      </Link>
                  </div>
              </div>
        );
    }
    renderContent(currentTrial) {
        return (
            <div className="row text-center ">
                <div className="col-md-8 col-md-offset-2">
                    <div className="display-sizing">
                        <img className="display-trial-image" src={currentTrial.logoLink} alt="" />
                    </div>
                    <p>{currentTrial.description? currentTrial.description : 'No description available yet'}</p>

                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { currentTrial } = this.props;

        if(!currentTrial) {
            return <div>Loading Trial...</div>;
        }
        return (
          <div id="wrap">
              {this.renderTopBanner(currentTrial)}
              <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
              <div id="main" className="container">
                  {this.renderContent(currentTrial)}
                  {this.renderButtons()}
                  <div className="col-md-8 col-md-offset-2">
                  <div className="main-text">
                     <p> * Becoming an official Tryall tester means that you will now be able to speak to your experience using this product to other Tryall users.<br/>
                     </p>
                  </div>
                  </div>
              </div>
          </div>
        );
    }
}


function mapStateToProps({ singleTrial }) {
    return {
        currentTrial: singleTrial
    };
}

export default connect(mapStateToProps, { fetchTrial,saveReminder,saveActiveTrial, becomeTester })(ConfirmationSubscription);
