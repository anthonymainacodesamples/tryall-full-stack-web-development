/**
 * Created by AnthonyMaina on 11/29/17.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { saveCategory, removeCategory } from '../../actions/index';



class SingleCategory extends Component {

    constructor(props) {
        super(props);
        this.toggleClass= this.toggleClass.bind(this);
        this.state = {
            active: this.props.isSelected,
        };
    }

    toggleClass(id) {
        const currentState = this.state.active;
        if(currentState === true) {
            this.props.removeCategory(id);
        } else {
            this.props.saveCategory(id);
        }
        this.setState({ active: !currentState });
    };


    render() {
        const { item } = this.props;
        return (
            <li className={ this.state.active ? " Product productActive" : "Product"} key={item._id} onClick={() => this.toggleClass(item._id)}>
                    <i  className={this.state.active ? `Product-icon Product-Icon-Selected fa ${item.browse_icon}` :`Product-icon fa ${item.browse_icon}`}/>
                    <h6 className={this.state.active ? "Product-name Product-Name-Selected" : "Product-name"}>
                        {item.name}
                    </h6>
            </li>
        );
    }
}

export default connect(null, { saveCategory, removeCategory  })(SingleCategory);