/**
 * Created by AnthonyMaina on 3/2/18.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Datetime from 'react-datetime';
import '../../styles/DateTimePicker.css';
import history from '../utils/history';
import SuccessIcon from '../../images/success.png';
import ProgressButton from 'react-progress-button'
import { connectRequest } from '../../actions/index';
import '../../styles/react-progress-button.css';
import AlertContainer from 'react-alert';





class SelectScheduleForConversation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            option1:'',
            option2:'',
            option3:'',
            buttonState: ''
        };

        this.handleOption1 = this.handleOption1.bind(this);
        this.handleOption2 = this.handleOption2.bind(this);
        this.handleOption3 = this.handleOption3.bind(this);

        this.finalSubmit = this.finalSubmit.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }


    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> One more thing...</h3>
                                <p className="sub-heading"> To help with scheduling, pick a time that you are free to talk to make sure you get to talk to the tester.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    renderTrialAndMatchInfo(currentTrial) {
        return (
            <div className="row text-center ">
                <div className="col-md-8 col-md-offset-2">
                    <div className="display-sizing">
                        <img className="display-trial-image" src={currentTrial.logoLink} alt="" />
                    </div>
                    <p>{currentTrial.description ? currentTrial.description : 'No description available yet'}</p>

                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                </div>
            </div>
        );
    }

    handleOption1(date){
        this.setState({
            option1:date.toDate()
        });
    }
    handleOption2(date){
        this.setState({
            option2:date.toDate()
        });
    }
    handleOption3(date){
        this.setState({
            option3:date.toDate()
        });
    }

    finalSubmit() {

        if(this.state.option1 !== '' && this.state.option2 !== '' && this.state.option3 !== '' )
        {
            this.setState({buttonState: 'loading'});

            // make asynchronous calls
            setTimeout(() => {
                const {trialId, testerId} = this.props.match.params;
                const optionsAvailable = {};

                if (this.state.option1) {
                    optionsAvailable.option1 = this.state.option1
                }
                if (this.state.option2) {
                    optionsAvailable.option2 = this.state.option2

                }
                if (this.state.option3) {
                    optionsAvailable.option3 = this.state.option3;
                }
                if (optionsAvailable) {

                    this.props.connectRequest(trialId, testerId, optionsAvailable)
                }
                this.setState({buttonState: 'success'})

            }, 1500);
            setTimeout(() => {
                history.push('/homefeed');
            }, 2000);
        }else {
            this.showAlert()
        }

    }

    alertOptions = {
        offset: 14,
        position: 'top right',
        theme: 'light',
        time: 5000,
        transition: 'scale'
    };

    showAlert = () => {
        this.msg.show('Please select 3 times', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    render() {

        // Let's use the static moment reference in the Datetime component
        var yesterday = Datetime.moment().subtract( 1, 'day' );
        var upperLimit = Datetime.moment().add(5, 'day');
        var valid = function( current ){
            return current.isAfter( yesterday ) && current.isBefore(upperLimit) ;
        };

        const { identifiedTesterInfo } = this.props;
        const { currentTrial } = this.props;
        const {singleTester, currentUserInfo} = identifiedTesterInfo;

        if(!singleTester || !currentTrial) {
            return (
                <div>
                    Loading...
                </div>
            );
        } else {
            return (
                <div id="wrap">
                    {this.renderTopBanner()}
                    <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                    <div id="main" className="container">
                        {this.renderTrialAndMatchInfo(currentTrial)}
                        <div className=" top-dash-line"/>
                        <div className="consent-content text-center">
                            <h4>Select the time at which you would like to speak to the tester.</h4>
                            <p className="main-text text-center">
                                Pick at least 3 times (within the next 5 days) during which you are free to talk to a tester. We will match you and once the time comes, simply come back to Tryall and you will be able to chat directly with them.
                            </p>
                        </div>


                        <div className="row">
                        <div className="col-md-4">
                            <Datetime isValidDate={ valid } value={this.state.option1} onChange={this.handleOption1}/>
                        </div>
                            <div className="col-md-4">
                                <Datetime isValidDate={ valid } value={this.state.option2} onChange={this.handleOption2} />
                            </div>
                            <div className="col-md-4">
                                <Datetime isValidDate={ valid } value={this.state.option3} onChange={this.handleOption3} />
                            </div>
                        </div>

                        <div className="text-center">
                            <ProgressButton onClick={this.finalSubmit} state={this.state.buttonState}>
                                Done
                            </ProgressButton>
                        </div>

                        {/*<div className="text-center">*/}
                                {/*<button*/}
                                    {/*className="dark-blue-general-button"*/}
                                    {/*onClick={()=> this.finalSubmit()}>*/}

                                    {/*Done*/}

                                {/*</button>*/}
                        {/*</div>*/}



                    </div>
                </div>
            );
        }
    }
}


function mapStateToProps({ identifiedTesterInfo,singleTrial  }) {
    return (
        {
            identifiedTesterInfo,
            currentTrial: singleTrial
        }
    );
}
export default connect(mapStateToProps, { connectRequest })(SelectScheduleForConversation);