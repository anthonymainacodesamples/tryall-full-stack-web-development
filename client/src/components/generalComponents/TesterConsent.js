/**
 * Created by AnthonyMaina on 2/27/18.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  OverlayTrigger, Tooltip } from 'react-bootstrap';
import { fetchTrial, becomeTester } from '../../actions/index';
import SuccessIcon from '../../images/success.png';
import AlertContainer from 'react-alert';
import history from '../utils/history';
import { Link } from 'react-router-dom';





class TesterConsent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isEligible:false
        };
        this.checkIfEligible = this.checkIfEligible.bind(this);
        this.becomeATester = this.becomeATester.bind(this);

    }

    alertOptions = {
        offset: 14,
        position: 'top right',
        theme: 'light',
        time: 5000,
        transition: 'scale'
    };

    componentDidMount() {
        const { id } = this.props.match.params;
        this.dataFetch(id);
        window.scrollTo(0, 0)
    }

    dataFetch(id) {
        this.props.fetchTrial(id);
    }

    showEligible = () => {
        this.msg.show('You are eligible.', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    showNotEligible = () => {
        this.msg.show('You are not eligible', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    checkIfEligible() {

        this.showEligible()
    }



    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Sign Up to be an Official Tryall Tester.</h3>
                                <p className="sub-heading"> Speak to fellow entrepreneurs. Make some money.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    renderContent(currentTrial) {
        return (
            <div className="row text-center ">
                <div className="col-md-8 col-md-offset-2">
                    <div className="display-sizing">
                        <img className="display-trial-image" src={currentTrial.logoLink} alt="" />
                    </div>
                    <p>{currentTrial.description? currentTrial.description : 'No description available yet'}</p>

                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            {currentTrial.DurationInDays} days</p>
                    </div>
                    <div className="col-xs-6 col-md-6">
                        <p className="lead">
                            ${currentTrial.monthlyDollarPrice}/month</p>
                    </div>
                </div>
            </div>
        );
    }

    becomeATester(id) {

        this.props.becomeTester(id);
        history.push(`/trials/${id}`);
    }

    renderSignUpButton() {
        const { currentTrial } = this.props;


        const tooltipIneligible = (
            <Tooltip id="tooltip">
                You are not yet eligible to become a tester.
            </Tooltip>
        );

        if(this.state.isEligible) {

            return (
                <OverlayTrigger placement="top" overlay={tooltipIneligible}>
                    <button
                        className="disabled-tester">
                        I agree. Sign me up as a tester.
                    </button>
                </OverlayTrigger>
            );
        } else {
            return (
                <div className="text-center">
                    <button
                        className="dark-blue-general-button large-trial-display"
                        onClick={() => this.becomeATester(currentTrial._id)}
                        >
                        I agree. Sign me up.
                    </button>
                    <Link to={'/homefeed'}>
                    <button
                        className="dark-blue-general-button invert-general large-trial-display"
                    >
                        No, thank you.
                    </button>
                    </Link>
                </div>
            );
        }
    }

    render() {


        const { currentTrial } = this.props;

        if(!currentTrial) {
            return <div>Loading Trial...</div>;
        }
        return (
          <div id="wrap">
              {this.renderTopBanner()}

              {this.renderContent(currentTrial)}


              <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
              <div className="container">

              <div className="consent-content">
              <p className="main-text">
                  <h4>What is expected as a Tryall Tester for {currentTrial.name}:</h4>
                  * You are willing to provide insightful information regarding your experience with this trial when you are matched with a tester. <br/>
                  * You will try to be unbiased as possible when sharing your experience.<br/>
                  * You will be honest regarding what worked for you and what didn’t.<br/>
                  * You will be respectful to the person you are matched to preserve the integrity of this community that you will now be a part of.<br/>
              </p>
                  <p className="main-text">
                      <h4>Requirements to be eligible:</h4>
                      * You have indicated that you are currently testing the product and it is available under your active trials.<br/>
                      * You have used it for a minimum eligibility period.<br/>
                  </p>
              </div>
                  <div className="row">
                      <div className=" top-dash-line"/>
                  <div className="text-center">
                      {this.renderSignUpButton()}
                  </div>
                  </div>


              </div>
              <div id="main" className="container"/>
          </div>
        );
    }
}

function mapStateToProps({ singleTrial }) {
    return {
        currentTrial: singleTrial
    };
}

export default connect(mapStateToProps, { fetchTrial, becomeTester })(TesterConsent);