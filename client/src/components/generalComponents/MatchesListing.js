/**
 * Created by AnthonyMaina on 3/3/18.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchMatches } from '../../actions/index';
import _ from 'lodash';
import MatchList from './MatchList';


class MatchesListing extends Component {


    componentDidMount() {
        this.props.fetchMatches();

    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Tester Conversations</h3>
                                <p className="sub-heading"> Here is a list of existing matches for specific trials. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderContent() {
        const { matchesList } = this.props;
        if(_.isEmpty(matchesList)) {
            return (
                <div className="text-center">
                    No matches made yet.
                </div>
            );
        } else {
            //1st layer
            const { finalObj, userMatches, testerMatches } = matchesList;
            const { selfDetails, testerTrials, userTrials } = finalObj;

            return (
                    <MatchList
                        testerTrialsList={testerTrials}
                        userTrialsList={userTrials}
                        selfInfo={selfDetails}
                        userMatchConnect={userMatches}
                        testerMatchConnect={testerMatches} />
            );
        }

    }

    render() {
        return (
          <div id="wrap">
              {this.renderTopBanner()}
              <div id="main" className="container">
                  {this.renderContent()}
              </div>

          </div>
        );
    }
}

function mapStateToProps({ matchesList }) {
    return ({ matchesList });
}

export default connect(mapStateToProps, {fetchMatches})(MatchesListing);