/**
 * Created by AnthonyMaina on 2/12/18.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { fetchCustom } from '../../actions/index';
import { Link } from 'react-router-dom';
import TrialList from './TrialList';


class PersonalSelection extends Component {



    componentWillMount() {
        this.props.fetchCustom();
        window.scrollTo(0, 0)


    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Personalized Category Recommendations</h3>
                                <p className="sub-heading"> Essential trials that best match your profile and company information. </p>
                            </div>
                            <div className="text-right search-cat-div ">
                                <p>
                                    <Link className="home-banner-button" to={`/update/company-info`} style={{ textDecoration: 'none' }}>
                                        Update Company Information
                                    </Link>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderCategories() {
        //Sort array alphabetically
        const interimArray = this.props.personalCategoryObject;
        const sortedArray = _.sortBy(interimArray, ({ categoryName }) => {
            return categoryName;
        });

       return  _.map(sortedArray,({ categoryName,trialsList}) => {
           if(categoryName) {
               return (
                   <div className="container" key={categoryName}>
                       <div className="page-header-homepage mini-header">
                           <div className="mini-title">
                               <p> { categoryName } </p>
                           </div>
                           <div className="mini-button">
                               <Link to={`/custom/${categoryName}`}>
                                   <button className="dark-blue-mini-button"> View More</button>
                               </Link>
                           </div>
                       </div>

                       <TrialList itemList={ _.take(trialsList, 3)}/>
                   </div>
               );
           }
        });

    }


    render() {
            return (
                <div id="wrap">
                    {this.renderTopBanner()}
                    <div id="main">
                        { this.renderCategories() }
                    </div>
                </div>
            );

    }
}

function mapStateToProps({ personalCategory }) {
    return { personalCategoryObject: personalCategory};
}

export default connect(mapStateToProps, { fetchCustom })(PersonalSelection);