/**
 * Created by AnthonyMaina on 2/5/18.
 */

import React, { Component } from 'react';

class LandingV2 extends  Component {


    renderBannerImage() {
        return (
            <div className="image">
                <div className="containing-top">
                    <div className="landing-logo"/>
                    <h1 className="heading">The best place to test SaaS products.</h1>
                    <div className="sub-heading-div text-center">
                        <p className="sub-heading"> This is Tryall. We seek to empower small businesses and entrepreneurs to make the best decisions when deciding on their next SaaS tool.</p>
                    </div>
                    <p><a className="get-started-button" href="/auth/google">GET STARTED HERE</a></p>
                </div>
            </div>
        );
    }

    renderHowItWorks() {
        return (
            <div>
                <div className="row">
                    <div className="text-center">
                        <h2 className="k-header--large k-color--marine k-spacing-top--huge k-block">How It Works</h2></div>
                </div>
                <div className="steps-wrapper container">
                    <section className="step-wrapper">
                        <div className="left-text-content">
                            <h3>1. Tell Us About Your Company</h3>
                            <p>
                                By providing us specific information regarding your company, industry and goals,
                                we can recommend the right software tools for you to test out.
                                This information is used on our database to ensure you make the right decision.
                            </p>
                        </div>
                        <div className="image-wrapper">
                        <div className="tell-us-image"/>
                        </div>
                    </section>
                    <section className="step-wrapper">
                    <div className="left-text-content">
                        <h3>2. Test out products matching your profile.</h3>
                        <p>
                            Once you settle on a free trial that you’d like to try out, we will keep track
                            of any associated dates and all you have to think about is your experience of the product.
                        </p>
                    </div>
                        <div className="image-wrapper">
                        <div className="test-out-image"/>
                        </div>

                    </section>

                    <section className="step-wrapper">
                    <div className="left-text-content">
                        <h3>3. Tap into a community of testers.</h3>
                        <p>
                            We give you access to people such as yourself who are looking to discover a new product.
                            This allows to hear from someone looking for insight into a product.
                        </p>
                    </div>
                        <div className="image-wrapper">
                        <div className="tap-into-image"/>
                        </div>
                    </section>

                </div>
            </div>
        );
    }

    renderMiddleBanner() {
        return (
          <section className="connect-banner">
                      <div className="container">
                          <div className="main-landing-wrapper">
                              <div className="sub-landing-text-div">
                                  <h4 className="heading"> Unsure of what SaaS product suits you best?</h4>
                                  <p className="sub-heading"> We pride ourselves on providing you with the best possible experience in testing a SaaS tool before deciding what you will commit to.
                                      </p>
                              </div>
                              <div className="text-right search-cat-div ">
                                  <p><a className="get-started-button landing-section-btn" href="/auth/google">Get Started Today</a></p>
                              </div>
                          </div>
                      </div>
          </section>
        );
    }


    renderAdditionalInfo() {
        return (
            <section className="w-under-the-hood k-spacing-padding-top--large k-spacing-bottom--huge">
                <div className="container">
                    <div className="row">
                        <div className="text-center k-color--marine k-spacing-bottom--huge k-block">
                            <h2 className="k-header--large k-color--marine k-spacing-bottom--x-large k-block">About Us</h2>
                            <p>
                            At Tryall, we aim to empower entrepreneurs and SME owners to make informed decisions on the necessary software tools that can help manage and grow their business.
                            We provide the best place to discover, test, and manage SaaS tools by offering personalized recommendations, an impartial and centralized community of testers, and a seamless way to track all your free trials.
                            </p>
                        </div>

                    </div>
                    <div className="row w-under-the-hood-row">
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--manage"/>
                            <h3 className="k-header">Smooth Management</h3>
                            <p className="k-paragraph">Tryall provides a simple and intuitive interface for you to browse and discover new products and keep track of all your subscriptions.</p>
                        </div>
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--recommend"/>
                            <h3 className="k-header">Recommendations</h3>
                            <p className="k-paragraph">As you keep using Tryall, we better learn what sort of products interest you thus providing you more valuable recommendations.</p>
                        </div>
                        <div className="col-sm-4">
                            <div className="w-under-the-hood-icon--reminder"/>
                            <h3 className="k-header">Safe Testing</h3>
                            <p className="k-paragraph">By allowing you to set custom reminders for your active free trials, you will always be notified right before your free trial ends to avoid unexpected charges.</p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    render() {
        return (
            <div>
                { this.renderBannerImage() }
                { this.renderHowItWorks() }
                {this.renderMiddleBanner()}
                {/*{ this.renderAboutUs() }*/}
                {this.renderAdditionalInfo()}
            </div>
        );
    }

}
export default LandingV2;