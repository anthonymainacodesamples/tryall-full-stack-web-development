/**
 * Created by AnthonyMaina on 10/5/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRecommended, fetchFeatured, fetchSelectedCategories  } from '../../actions/index';
import _ from 'lodash';
import { Glyphicon} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import TrialList from './TrialList';
import '../../styles/searchResults.css';
import '../../styles/HomeFeed.css';


class Homefeed extends Component {

    componentDidMount() {
        this.props.fetchRecommended(true);
        this.props.fetchFeatured(true);
        this.props.fetchSelectedCategories();
            window.scrollTo(0, 0)
    }

    renderRecommended() {
        const itemList = this.props.recommended;
        return (
           <TrialList itemList={itemList} />

       );
    }

    renderFeatured() {
        const itemList = this.props.featured;
        return (
            <TrialList itemList={itemList} />
        );
    }

    renderFeaturedHeader() {
        return (
            <div className="page-header-homepage">
                <p> Featured Free Trials </p>
            </div>
        );
    }
    renderRecommendedHeader() {
        return (
            <div className="page-header-homepage">
                <p> Trials you expressed interest in </p>
            </div>
        );
    }
    renderMiddleBanner() {
        return (
            <section className="connect-banner">
                <div className="container">
                    <div className="main-landing-wrapper">
                        <div className="sub-landing-text-div">
                            <h4 className="heading"> Unsure of what SaaS product suits you best?</h4>
                            <p className="sub-heading"> We pride ourselves on providing you with the best possible experience in testing a SaaS tool before deciding what you will commit to.
                            </p>
                        </div>
                        <div className="text-right search-cat-div ">
                            <p><a className="get-started-button landing-section-btn" href="">Our Community</a></p>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
    renderTopButtons() {
        return (
            <div className="text-center">
                <h3> Discover the best place to test out subscription based products.</h3>
          <div className="row">
                  <Link to={`/discover`}  style={{ textDecoration: 'none' }}>
                      <button
                          className="browsecat-button"
                      >
                          <Glyphicon glyph="search"/> Begin Search
                      </button>
                  </Link>
                  <Link to={`/browsecategories`}  style={{ textDecoration: 'none' }}>
                      <button
                          className="browsecat-button"
                      >
                          <Glyphicon glyph="th"/> Browse Categories
                      </button>
                  </Link>
              </div>
            </div>
        );
    }

    renderHelperMessage() {

        if(_.size(this.props.recommended) < 6)  {
            return (
                <p className="small-helper-text">
                    To receive more recommendations, update your followed categories.
                </p>
            );
        }
    }
    renderTopBanner() {
        return (
            <div className="homefeed-banner">
                <div className="homefeed-top">
                    <div className="container">
                    <h3 className="heading"> Welcome to the best place to test out subscription based products.</h3>
                    <div className="sub-home-text-div">
                        <p className="sub-heading"> You can browse our extensive collection, search for trials and manage your free trial subscriptions.</p>
                    </div>
                        <div className="text-center ">

                        <p>
                            <Link className="home-banner-button" to={`/discover`} style={{ textDecoration: 'none' }}>
                            Begin Search
                            </Link>
                            <Link className="home-banner-button" to={`/browsecategories`} style={{ textDecoration: 'none' }}>
                            Browse Categories
                            </Link>
                        </p>
                        </div>

                    </div>
                </div>
            </div>
        );
    }

    render() {

        if(!this.props.recommended) {
            return;
        }

        return (
            <div >
                {this.renderTopBanner() }
                <div className="container">
                {/*{this.renderTopButtons()}*/}
                {this.renderFeaturedHeader()}
                <div className="row">
                    {this.renderFeatured()}
                </div>
                <div className="text-center">
                    <Link to={`/featured`}  style={{ textDecoration: 'none' }}>
                        <button className="dark-blue-general-button k-button--mobile-full-width">More Featured</button>
                    </Link>
                </div>
                </div>
                    {this.renderMiddleBanner()}
                    <div className="container">
                {this.renderRecommendedHeader()}
                <div className="row">
                    {this.renderRecommended()}
                </div>
                <div className="text-center">
                    <Link to={`/recommended`}  style={{ textDecoration: 'none' }}>
                        <button className="dark-blue-general-button large-trial-display">More trials by preferences</button>
                    </Link>
                </div>
                </div>
            </div>

        );
    }

}
function mapStateToProps({ recommended, featured }) {
    return { recommended, featured };
}

export default connect(mapStateToProps,{ fetchRecommended, fetchFeatured, fetchSelectedCategories })(Homefeed);