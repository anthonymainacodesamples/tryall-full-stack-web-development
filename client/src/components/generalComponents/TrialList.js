/**
 * Created by AnthonyMaina on 10/23/17.
 */
import React, { Component } from 'react';
import TrialItem from './TrialItem';
import _ from 'lodash';
import '../../styles/trialList.css'


class TrialList extends Component {

    render() {
        const { itemList, testerCheck } = this.props;
        return _.map(itemList, item => {
            return (
                <div className="item  col-xs-12 col-sm-6 col-md-4" key={item._id}>
                    <TrialItem item={item} removeFromSaved={ this.props.removeFromSaved } testerCheckArray={testerCheck} />
                </div>
            );
        });
    }
}

export default TrialList;