/**
 * Created by AnthonyMaina on 2/13/18.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import TrialList from './TrialList';


class PersonalSingleCategory extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)


    }

    renderTopBanner() {

        const { categoryName } = this.props.match.params;
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Personalized Category: { categoryName } </h3>
                                <p className="sub-heading"> Here are the trials we recommend based on your profile information. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderTrials() {
        const { categoryName } = this.props.match.params;
        const interimArray = this.props.personalCategoryObject;
        const itemObj = _.find(interimArray, { categoryName });


        return (
            <TrialList itemList={itemObj.trialsList} />
        );
    }

    render() {
        return (
          <div id="wrap">
              {this.renderTopBanner()}
              <div id="main" className="container">
                  {this.renderTrials()}
              </div>
          </div>
        );
    }
}
function mapStateToProps({ personalCategory }) {
    return { personalCategoryObject: personalCategory};
}

export default connect(mapStateToProps)(PersonalSingleCategory);