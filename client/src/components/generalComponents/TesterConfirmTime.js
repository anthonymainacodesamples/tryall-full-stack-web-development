/**
 * Created by AnthonyMaina on 3/3/18.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { confirmTime } from '../../actions/index';
import { Link } from 'react-router-dom';

class TesterConfirmTime extends Component {


    componentDidMount() {
        const { trialId, testerId, userId, optionNumber } = this.props.match.params;
        this.props.confirmTime(trialId,testerId,userId,optionNumber);
    }


    renderTopBanner() {

        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Confirmed.</h3>
                                <p className="sub-heading"> Your selection has been noted and you will be notified when the time comes. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    render(){
        return (
          <div id="wrap">
              {this.renderTopBanner()}
              <div id="main" className="container">
                  <div className="text-center consent-content">
                      <Link to={`/`}>
                          <button
                              className="dark-blue-general-button invert-general large-trial-display">
                              Click here to keep browsing.
                          </button>
                      </Link>
                  </div>

              </div>
          </div>
        );
    }
}

export default connect(null, { confirmTime })(TesterConfirmTime);