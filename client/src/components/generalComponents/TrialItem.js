/**
 * Created by AnthonyMaina on 10/8/17.
 */
import React, { Component } from 'react';
import {ButtonToolbar,Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';
// ES6
import * as Vibrant from 'node-vibrant'
import { fetchSaved,saveTrial,removeFromSaved,fetchTrial,postClicks} from '../../actions/index';
import '../../styles/trialList.css';



class TrialItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            savedChange:false,
            color: [50, 100, 150]
        };
        this.saveTrial = this.saveTrial.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }


    // chooseColor () {
    //     for (var i = 0, random = []; i < 3; i++) {
    //         random.push(Math.floor(Math.random()*256));
    //     }
    //     return random;
    // }

    formatColor(ary) {
        return 'rgb(' + ary.join(', ') + ')';
    }

    componentDidMount() {

        const { item} = this.props;
        //
        // let v = new Vibrant(`${item.logoLink}`, {
        //     'Access-Control-Allow-Origin': 'http://localhost:3000',
        //     'Access-Control-Allow-Credentials': 'true'
        // });
        // v.getPalette((err, palette) => {
        //     if(palette && palette.Vibrant) {
        //         // console.log("Works for ", item.name);
        //         this.setState({
        //            color: palette.Vibrant._rgb
        //         });
        //     } else {
        //         // console.log("Doesn't work for ", item.name);
        //     }
        // });
    }

    saveTrial(id) {
        this.setState({savedChange:false});
        this.props.saveTrial(id);
        setTimeout(() => {
            // Completed of async action, set loading state back
            this.setState({savedChange: false});
        }, 1000);

    }

    handleClick(id) {
        this.setState({savedChange: true});
        // This probably where you would have an `ajax` call
        this.props.removeFromSaved(id);
        setTimeout(() => {
            // Completed of async action, set loading state back
            this.setState({savedChange: false});
        }, 1000);
    }

    renderButtons(id) {
        const { savedTrials } = this.props;

        if(savedTrials[id]) {
            return (
                <ButtonToolbar>
                    <Link to={`/trials/${id}`}>
                        <Button>View Free Trial</Button>
                    </Link>
                    <Button
                        bsStyle={ savedTrials[id] ? "danger" : "primary" }
                        disabled={this.state.savedChange}
                        className="pull-right"
                        onClick={!this.state.savedChange ? () => this.handleClick(id) : null}
                    > { this.state.savedChange ? 'Removing...' : 'Remove'} </Button>
                </ButtonToolbar>
            );
        }

        return(
            <ButtonToolbar>
                <Link to={`/trials/${id}`}>
                    <Button>View Free Trial</Button>
                </Link>
                <Button bsStyle="primary" className="pull-right" onClick={() => this.saveTrial(id)}>Save For Later</Button>
            </ButtonToolbar>
        );

    }

    renderTag(id) {

        const { eligibilityItems } = this.props;
        if(eligibilityItems) {
             if (eligibilityItems[id]) {
                    return (
                        <div className=" match-badge match-badge-danger match-badge-outlined">TESTER</div>
                    );
                }

        }
    }


    render() {
        const { item  } = this.props;
        const id = item._id;
        const imageLink = item.logoLink;
        // Promise
        // Vibrant.from(`${item.logoLink}`).getPalette()
        //     .then((palette) => console.log("Yo broo",palette.Vibrant));
        // Using constructor



        return (
            <Link to={`/trials/${id}`} onClick={() => this.props.postClicks(id)} style={{ textDecoration: 'none' }}>
                <div className="thumbnail" style={{ borderLeftColor:this.formatColor(this.state.color) }}>
                    <img className="group list-group-image" src={imageLink} alt="" />
                    <div className="caption">
                        <h4 className="group inner list-group-item-heading text-center title-space">
                            {item.name}</h4>
                        <p className="group inner list-group-item-text trial-panel text-center">
                            {item.description? item.description : 'No description is available yet'} </p>
                        <div className="row">
                            <div className="col-xs-5 col-sm-5 text-center">

                                <p className="duration"> {item.DurationInDays} days</p>
                            </div>
                            <div className="col-xs-2 col-sm-2 text-center badge-position">
                                {this.renderTag(id)}
                            </div>
                            <div className="col-xs-5 col-sm-5 text-center">
                                <p className="cost"> ${item.monthlyDollarPrice}/month</p>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

function mapStateToProps({ savedTrials,eligibilityItems}) {
    return {savedTrials,eligibilityItems};
}

export default connect(mapStateToProps,{ fetchSaved, saveTrial, removeFromSaved,fetchTrial,postClicks})(TrialItem);