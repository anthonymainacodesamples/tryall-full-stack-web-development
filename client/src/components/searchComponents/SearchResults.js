/**
 * Created by AnthonyMaina on 10/6/17.
 */

import React, { Component } from 'react';
import SearchBar from './SearchBar';
import { Link } from 'react-router-dom';
import { Glyphicon } from 'react-bootstrap';
import SearchItems from './SearchItems';
import '../../styles/GeneralBanner.css';



class SearchResults extends Component {

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                        <div className="sub-search-text-div">
                            <h3 className="heading"> What are you looking for today?</h3>
                            <p className="sub-heading"> Search based on specific categories or trials.</p>
                        </div>
                        <div className="text-right search-cat-div ">
                            <p>
                                <Link className="home-banner-button" to={`/browsecategories`} style={{ textDecoration: 'none' }}>
                                    <Glyphicon glyph="th"/> Browse All Categories
                                </Link>
                            </p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    render() {
        return (
            <div id="wrap">
                {this.renderTopBanner()}
            <div id="main" className="container main-content clear-top" style = {{ textAlign:'center' }}>
                <div className="row ">
                    <SearchBar />
                </div>
                <div>
                    <SearchItems />
                </div>
            </div>
            </div>
        );
    }

}

export default SearchResults;