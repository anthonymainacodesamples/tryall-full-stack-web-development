/**
 * Created by AnthonyMaina on 10/6/17.
 */

import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators}  from 'redux';
import { fetchResults, searchLogging } from '../../actions/index';
import '../../styles/SearchPageStyling.css';


class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.state = {term:''};
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit =  this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({term: event.target.value});
    }
    onFormSubmit(event) {
        event.preventDefault();

        const searchQuery = this.state.term;
        this.props.fetchResults(searchQuery);
        this.props.searchLogging(searchQuery);
        this.setState({ term:'' });
    }
    render() {
        return(
            <div className="row">
                <div className="col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2">
                    <form onSubmit={this.onFormSubmit} className="searchbox">
                        <div className="searchbox__wrapper">
                        <input
                            className="searchbox__input"
                            placeholder="Search by name or category"
                            value = {this.state.term}
                            onChange={this.onInputChange}
                        />
                            <button type="submit" className="searchbox__submit"><i  className="fa fa-search"/></button>
                            <button type="reset"  className="searchbox__reset hide"><i className="fa fa-times-circle-o"/></button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchResults, searchLogging }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);