/**
 * Created by AnthonyMaina on 10/8/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { Glyphicon } from 'react-bootstrap';
import TrialList from '../generalComponents/TrialList';
import '../../styles/SearchPageStyling.css';

class SearchItems extends Component {


    renderCategoriesButton() {
        return (
        <div>
            <Link to={`/browsecategories`}  style={{ textDecoration: 'none' }}>
                <button
                    className="browsecat-button"
                    >
                <Glyphicon glyph="th"/> Browse Categories
                </button>
            </Link>
        </div>
        );
    }

    renderResultLabel(search) {

        const sizeVal = _.size(search);

        if(sizeVal === 0 ) {
        } else {
            if (sizeVal === 1) {
                return (
                    <h4> Found {_.size(search)} result.</h4>
                );
            } else {
                return (
                <h4>
                    Found {_.size(search)} results.
                </h4>
                )
            }
        }
    }

    render() {
        const { search } = this.props;
        return(
            <div>
                {this.renderResultLabel(search)}
                <div className="row results-body">
                    <TrialList itemList={ search } />
                </div>
            </div>
        );
    }
}

function mapStateToProps({ search }) {
    return { search };
}

export default connect(mapStateToProps)(SearchItems);