/**
 * Created by AnthonyMaina on 10/4/17.
 */


import React, { Component } from 'react';
import { Route, Switch,Router} from 'react-router-dom';
import { connect } from 'react-redux';
import createHistory from './utils/history';
import * as actions from '../actions';
import Homefeed from './generalComponents/Homefeed';
import SearchRes from './searchComponents/SearchResults';
import TrialDisplay from './generalComponents/TrialDisplay';
import SavedTrials from './myTrialsComponents/SavedTrials';
import PastTrials from './myTrialsComponents/PastTrials';
import ActiveTrials from './myTrialsComponents/ActiveTrials';
import PersonalSettings from './settingsComponents/PersonalSettings';
import PopularFreeTrials from './generalComponents/PopularFreeTrials';
import RecommendedTrials from './generalComponents/RecommendedTrials';
import FeaturedTrials from './generalComponents/FeaturedTrials';
import CategoriesSelection from './generalComponents/CategoriesSelection';
import FailureToSubscribe from './generalComponents/FailureToSubscribe';
import BrowseCategories from './generalComponents/BrowseCategories';
import CategoryPage from './generalComponents/CategoryPage';
import ReselectCategories from './generalComponents/ReselectCategories';
import LandingV2 from './generalComponents/LandingV2';
import TermsAndConditions from './generalComponents/TermsAndConditions';
import PrivacyPolicy from './generalComponents/PrivacyPolicy';
import ScrollToTop from './utils/ScrollToTop';
import '../styles/overallSiteStickFooter.css';
import './generalComponents/Landing';
import HeaderV2 from "./navItems/HeaderV2";
import FooterV2 from './navItems/FooterV2';
import OpeningSurvey from './OpeningSurvey/OpeningSurvey';
import PersonalSelection from './generalComponents/PersonalSelection';
import PersonalSingleCategory from './generalComponents/PersonalSingleCategory';
import UpdateSurveyDetails from './OpeningSurvey/UpdateSurveyDetails';
import TesterMatch from './generalComponents/TesterMatch';
import TesterConsent from './generalComponents/TesterConsent';
import ConfirmationSubscription from './generalComponents/ConfirmationSubscription';
import { spring,AnimatedSwitch } from 'react-router-transition';
import ItemSchedule from './generalComponents/SelectScheduleForConversation';
import TesterConfirmTime from './generalComponents/TesterConfirmTime';
import MatchesListing from './generalComponents/MatchesListing';




// we need to map the `scale` prop we define below
// to the transform style property
function mapStyles(styles) {
    return {
        opacity: styles.opacity,
        transform: `scale(${styles.scale})`,
    };
}

// wrap the `spring` helper to use a bouncy config
function bounce(val) {
    return spring(val, {
        stiffness: 330,
        damping: 22,
    });
}

// child matches will...
const bounceTransition = {
    // start in a transparent, upscaled state
    atEnter: {
        opacity: 0,
        scale: 1.2,
    },
    // leave in a transparent, downscaled state
    atLeave: {
        opacity: bounce(0),
        scale: bounce(0.8),
    },
    // and rest at an opaque, normally-scaled state
    atActive: {
        opacity: bounce(1),
        scale: bounce(1),
    },
};

class App extends Component {

    componentDidMount() {
        this.props.fetchUser();
    }

    render() {
        return (
            <div>
                <Router history={createHistory} >
                    <div>
                        <HeaderV2 />
                        <Switch>
                            <Route exact path="/matches" component={ MatchesListing } />
                            <Route exact path="/tester-confirm/:trialId/:testerId/:userId/:optionNumber" component={TesterConfirmTime} />
                            <Route exact path="/user/schedule/:trialId/:testerId" component={ItemSchedule} />
                            <Route exact path="/user/confirm/:id" component={ConfirmationSubscription} />
                            <Route exact path="/tester/subscribe/:id" component={TesterConsent} />
                            <Route exact path="/match/:id" component={TesterMatch} />
                            <Route exact path="/update/company-info" component={UpdateSurveyDetails} />
                            <Route exact path="/custom/:categoryName" component = {PersonalSingleCategory} />
                            <Route exact path="/custom-selection" component = {PersonalSelection} />
                            <Route exact path="/info-survey" component = {OpeningSurvey} />
                            <Route exact path="/privacy-policy" component = {PrivacyPolicy} />
                            <Route exact path="/terms-conditions" component = {TermsAndConditions} />
                            <Route exact path="/update/categories" component={ ReselectCategories } />
                            <Route exact path="/category/:id" component={ CategoryPage } />
                            <Route exact path="/browsecategories" component={ BrowseCategories } />
                            <Route exact path="/subfail/:id" component={ FailureToSubscribe } />
                            <Route exact path ="/categories" component={CategoriesSelection}/>
                            <Route exact path="/popular" component={PopularFreeTrials}/>
                            <Route exact path="/recommended" component={RecommendedTrials} />
                            <Route exact path="/featured" component={FeaturedTrials} />
                            <Route exact path="/settings" component={PersonalSettings} />
                            <Route exact path="/trials/saved" component={SavedTrials} />
                            <Route exact path="/trials/active" component={ActiveTrials} />
                            <Route exact path="/trials/past" component={PastTrials} />
                            <ScrollToTop>
                                <Route exact path ="/categories" component={CategoriesSelection}/>
                                <Route exact path="/trials/:id" component={TrialDisplay} />
                                <Route path="/homefeed" component={Homefeed} />
                                <Route path="/discover" component={SearchRes} />
                                <Route exact path="/" component={LandingV2} />
                            </ScrollToTop>
                        </Switch>
                        <FooterV2/>
                    </div>
                </Router>
            </div>
        );
    }
};
export default connect(null, actions)(App);