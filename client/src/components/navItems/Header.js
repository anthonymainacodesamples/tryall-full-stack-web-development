/**
 * Created by AnthonyMaina on 10/5/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import whiteMark from '../../images/whiteMark.png';


class Header extends Component {



    renderContent() {
        switch (this.props.auth) {
            case null:
                return;
            case false:
                return (
                <Navbar.Collapse>
                    <Nav pullRight>
                        <NavItem eventKey={1} href="/auth/google"> <i className="fa fa-sign-in fa-fw"/>  Google Login </NavItem>
                    </Nav>
                </Navbar.Collapse>
                );

            default:
                return (
                    <Navbar.Collapse>
                        <Nav pullRight>
                            <LinkContainer
                                to="/discover"
                            >
                                <NavItem eventKey={1} href="">
                                    <i className="fa fa-search fa-fw"/> Search
                                </NavItem>
                            </LinkContainer>
                            <NavDropdown
                                eventKey={4}
                                title={<span><i className="fa fa-bookmark-o fa-fw"/> My Trials</span>}
                                id="basic-nav-dropdown">
                                <LinkContainer
                                    to="/trials/active"
                                >
                                    <MenuItem eventKey={4.1}>
                                        Active Trials
                                    </MenuItem>
                                </LinkContainer>
                                <LinkContainer
                                    to="/trials/saved"
                                >
                                    <MenuItem eventKey={4.1}>
                                        Saved Trials
                                    </MenuItem>
                                </LinkContainer>
                                <LinkContainer
                                    to="/trials/past"
                                >
                                    <MenuItem eventKey={4.2}>
                                        Past Trials
                                    </MenuItem>
                                </LinkContainer>
                            </NavDropdown>
                            <NavDropdown eventKey={3} title={<span><i className="fa fa-user-o fa-fw"/> My Profile</span>} id="basic-nav-dropdown">
                                <LinkContainer
                                    to="/settings"
                                >
                                <MenuItem eventKey={3.1}>
                                        Settings
                                </MenuItem>
                                </LinkContainer>
                                <MenuItem divider />
                                <MenuItem eventKey={3.2} href="/api/logout">Logout</MenuItem>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                );
        }
    }

    render() {

        return(
            <Navbar >
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link
                            to={this.props.auth ? "/homefeed" : "/"}
                        >
                            <div className="navbar-brand">
                                <img
                                    style={{ maxHeight: "15px" , marginTop: "-20px" }}
                                     src={whiteMark}
                                    alt="whiteLogoMark"
                                />
                            </div>
                            Tryall Beta
                        </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                {this.renderContent()}
            </Navbar>

        );

    }
}

function mapStateToProps({ auth }) {
    return { auth };
};


export default connect(mapStateToProps)(Header);