/**
 * Created by AnthonyMaina on 2/8/18.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'
;import '../../styles/footerStyling.css';

class FooterV2 extends Component {

    renderButton() {
        switch (this.props.auth) {
            case null:
                return;
            case false:
                return (
                    <a className="get-started-footer" href="/auth/google">Get Started</a>
                );
            default:
                return (
                    <a className="get-started-footer" href="/homefeed">Home</a>
                );
        }

    }


    render() {
        return (

            <footer id="fh5co-footer" className="fh5co-bg footer-stick" role="contentinfo">
                <div className="overlay"/>
                <div className="container">
                    <div className="row row-pb-md">
                        <div className="col-md-4 fh5co-widget">
                            <h3>This is Tryall</h3>
                            <p>Tryall provides the best possible platform to discover and test out subscription based products for your business.</p>
                            {this.renderButton()}
                        </div>
                        <div className="col-md-8">
                            <h3>Information</h3>
                            <div className="col-md-4 col-sm-4 col-xs-6">
                                <ul className="fh5co-footer-links">
                                    <li><a href="#">How It Works</a></li>
                                    <li><a href="#">About Us</a></li>
                                    <li>
                                        <Link to={`/terms-conditions`}  style={{ textDecoration: 'none' }}>
                                        Terms
                                        </Link>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-md-4 col-sm-4 col-xs-6">
                                <ul className="fh5co-footer-links">
                                    <li><a href="https://www.instagram.com/tryallapp/">Instagram</a></li>
                                    <li><a href="https://twitter.com/tryallapp">Twitter</a></li>
                                </ul>
                            </div>

                            <div className="col-md-4 col-sm-4 col-xs-6">
                                <ul className="fh5co-footer-links">
                                    <li>
                                        <p><i className="fa fa-envelope"/> hello@tryall.co</p>
                                    </li>
                                    <li>
                                        <a href="//www.iubenda.com/privacy-policy/40315217"
                                           className="iubenda-white iubenda-embed"
                                           title="Privacy Policy">
                                            Privacy Policy
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="row copyright">
                        <div className="col-md-12 text-center">
                            <p>
                                <small className="block">&copy; 2018 | All Rights Reserved.</small>
                                <small className="block">Asili Labs, LLC</small>
                            </p>
                        </div>
                    </div>

                </div>
            </footer>
        );
    }
}


function mapStateToProps({ auth }) {
    return { auth };
};

export default connect(mapStateToProps)(FooterV2);