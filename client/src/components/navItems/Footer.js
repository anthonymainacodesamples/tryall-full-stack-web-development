/**
 * Created by AnthonyMaina on 10/6/17.
 */

import React, { Component } from 'react';
class Footer extends Component {

    render() {
        return(
            <footer className=" footer-stick k-footer k-spacing-padding-top--xx-small k-spacing-top--xx-large">
                <div className="k-footer-social">
                    <div className="container">
                        <h3 className="k-footer-social-accounts-header">Find us on</h3>
                        <ul className="k-footer-social-accounts">
                            <li className="k-footer-social-account">
                                <a className="k-footer-social-account-link k-color--twitter" href="https://www.twitter.com/AsiliLabs" target="_blank" rel="noopener noreferrer">
                                    <div className="k-icon--twitter"/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="k-footer-copyright">
                    <div className="container">
                        <p className="k-paragraph--small k-spacing-bottom--none k-align--center k-color--gray-inactive">&copy; Asili Labs, LLC.</p>
                    </div>

                </div>
            </footer>
        );
    }
}
export default Footer;