/**
 * Created by AnthonyMaina on 2/5/18.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import '../../styles/MainNavGroup/MainNavStyling.css';



class HeaderV2 extends Component {


    renderContent() {
        switch (this.props.auth) {
            case null:
                return;
            case false:
                return (
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href="#">How It Works</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Community</a></li>
                        <li><a className="special-link" href="/auth/google">Log In</a></li>
                    </ul>
                );
            default:
                return (
                    <ul className="nav navbar-nav navbar-right">
                        <LinkContainer
                            to="/discover"
                        >
                            <NavItem eventKey={1} href="">
                                Search
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to="/custom-selection">
                            <NavItem eventKey={2} href="">
                            Recommendations <span className="badge badge-success">NEW</span>
                            </NavItem>
                        </LinkContainer>
                        <LinkContainer to="/matches">
                            <NavItem eventKey={6} href="">
                                Testers
                            </NavItem>
                        </LinkContainer>

                        <NavDropdown
                            eventKey={4}
                            title="My Trials"
                            id="basic-nav-dropdown">
                            <LinkContainer
                                to="/trials/active"
                            >
                                <MenuItem eventKey={4.1}>
                                    Active Trials
                                </MenuItem>
                            </LinkContainer>
                            <LinkContainer
                                to="/trials/saved"
                            >
                                <MenuItem eventKey={4.1}>
                                    Saved Trials
                                </MenuItem>
                            </LinkContainer>
                            <LinkContainer
                                to="/trials/past"
                            >
                                <MenuItem eventKey={4.2}>
                                    Past Trials
                                </MenuItem>
                            </LinkContainer>
                        </NavDropdown>
                        <NavDropdown eventKey={3} title={<i className="fa fa-user-circle-o" />} id="basic-nav-dropdown">
                            <LinkContainer
                                to="/settings"
                            >
                                <MenuItem eventKey={3.1}>
                                    Settings
                                </MenuItem>
                            </LinkContainer>
                            <MenuItem divider />
                            <MenuItem eventKey={3.2} href="/api/logout">Logout</MenuItem>
                        </NavDropdown>
                    </ul>
                );
        }
    }
    
    
    render() {
        return (
            <nav className="navbar-default">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>

                        </button>
                        <Link className="navbar-brand"
                            to={this.props.auth ? "/homefeed" : "/"}
                        >
                        Tryall Beta
                        </Link>
                    </div>
                    <div id="navbar" className="collapse navbar-collapse">
                        { this.renderContent() }
                    </div>
                </div>
            </nav>
        );
    }


}


function mapStateToProps({ auth }) {
    return { auth };
    };
export default connect(mapStateToProps)(HeaderV2);

