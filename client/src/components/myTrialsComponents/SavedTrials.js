/**
 * Created by AnthonyMaina on 10/21/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSaved } from '../../actions/index';
import TrialList from '../generalComponents/TrialList';



class SavedTrials extends Component {

    componentDidMount() {
        this.props.fetchSaved();
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Saved Trials</h3>
                                <p className="sub-heading"> Here are the products you have saved for later.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        const itemList = this.props.savedTrials;

        return(
            <div id="wrap">
                {this.renderTopBanner()}
            <div id="main" className="container">
                <TrialList itemList={ itemList }/>
            </div>
            </div>
        );
    }
}

function mapStateToProps({ savedTrials }) {
    return { savedTrials };
}

export default connect(mapStateToProps,{ fetchSaved })(SavedTrials);