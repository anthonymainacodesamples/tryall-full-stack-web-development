/**
 * Created by AnthonyMaina on 10/21/17.
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPast } from '../../actions/index';
import TrialList from '../generalComponents/TrialList';

class PastTrials extends Component {

    componentDidMount() {
        this.props.fetchPast();
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Past Trials</h3>
                                <p className="sub-heading"> Here are products that you have tested in the past.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        const { pastTrials } = this.props;


        return(
            <div id="wrap">
                {this.renderTopBanner()}
                <div id="main"  className="container">
                <TrialList itemList={ pastTrials } />
                </div>
            </div>
        );
    }

}

function mapStateToProps({ pastTrials }) {
    return({ pastTrials } );
}


export default connect(mapStateToProps,{ fetchPast })(PastTrials);