/**
 * Created by AnthonyMaina on 10/21/17.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchActive,fetchEligibility } from '../../actions/index';
import TrialList from '../generalComponents/TrialList';


class ActiveTrials extends Component {

    componentDidMount() {
        this.props.fetchActive();
        this.props.fetchEligibility();
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Active Trials</h3>
                                <p className="sub-heading"> Here are the products that you are currently testing.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        const { activeTrials } = this.props;
        return(
            <div id="wrap">
                {this.renderTopBanner()}
                <div id="main" className="container">
                <TrialList itemList={ activeTrials.trials } testerCheck={activeTrials.trialsTesterIds} />
                </div>
            </div>
        );
    }
}

function mapStateToProps({ activeTrials }) {
    return { activeTrials };
}

export default connect(mapStateToProps,{ fetchActive,fetchEligibility })(ActiveTrials);