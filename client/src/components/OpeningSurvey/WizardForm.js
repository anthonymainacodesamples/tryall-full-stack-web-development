/**
 * Created by AnthonyMaina on 2/11/18.
 */

import React, { Component } from 'react';

import WizardFormFirstPage from './WizardFormFirstPage';
import { connect } from 'react-redux';

class WizardForm extends Component {

    constructor(props) {
        super(props);
        this.nextPage = this.nextPage.bind(this);
        this.previousPage = this.previousPage.bind(this);
        this.state = {
            page: 1,
        };
    }
    nextPage() {
        this.setState({ page: this.state.page + 1 });
    }

    previousPage() {
        this.setState({ page: this.state.page - 1 });
    }


    render() {
        const { onSubmit } = this.props;

        return (
            <div className="container">
                <div>
                    <WizardFormFirstPage onSurveySubmit={onSubmit} introQuestions={ this.props.introQuestions } />
                </div>

            </div>
        );
    }
}

function mapStateToProps({ introQuestions }) {

    return { introQuestions };
}


export default connect(mapStateToProps)(WizardForm);