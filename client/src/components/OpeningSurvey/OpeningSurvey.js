/**
 * Created by AnthonyMaina on 2/11/18.
 */


import React, { Component } from 'react';
import WizardForm from './WizardForm';
import { connect } from 'react-redux';
import history from '../utils/history';
import { withRouter } from 'react-router-dom';
import { fetchOpeningQuestions,submitResults } from '../../actions/index';


class OpeningSurvey extends Component {

    componentWillMount() {
        this.props.fetchOpeningQuestions();
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Tell Us About Your Company</h3>
                                <p className="sub-heading"> This information will help us provide you with the best SaaS tools.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleSubmit = (values) => {
        this.props.submitResults(values,history);
        history.push('/categories')

    };
    render() {
        return (
          <div>
              { this.renderTopBanner()}
              <WizardForm onSubmit={ this.handleSubmit }/>
          </div>
        );
    }
}

export default connect(null, { fetchOpeningQuestions, submitResults })(withRouter(OpeningSurvey));