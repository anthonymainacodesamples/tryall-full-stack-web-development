/**
 * Created by AnthonyMaina on 2/11/18.
 */
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from './validate';
import _ from 'lodash';


class WizardFormFirstPage extends Component {



    renderQuestions() {
        const { introQuestions } = this.props;
        return _.map ( introQuestions, ({ prompt,_id,options }) => {
            return (
                <div key={_id} className="form-style">
                    <p>{ prompt }</p>
                    <Field
                        name={_id}
                        key={_id}
                        component={ ({ input, meta: { touched, error } }) => {
                                return (
                                    <div>
                                        <select {...input} className="select-styling">
                                            <option value="">Select an Option </option>
                                            {
                                                options.map(val => <option value={val._id} key={val._id}>{val.response}</option>)
                                            }
                                        </select>
                                        {touched && error && <span>{error}</span>}
                                    </div>
                                );
                            }
                        }
                        type="text"
                         />
                </div>
            );
        });

    }

    render() {

        return (
            <form onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}>
                {this.renderQuestions()}
                <div>
                    <button
                        type="submit"
                        className="dark-blue-general-button large-trial-display">
                        Save and Get Started
                    </button>
                </div>
            </form>
        );
    }
}

export default reduxForm({
    form: 'surveyForm', //             <------ same form name
    destroyOnUnmount: false, //        <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
    validate,
})(WizardFormFirstPage);

