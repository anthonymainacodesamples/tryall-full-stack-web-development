/**
 * Created by AnthonyMaina on 2/15/18.
 */


import React, { Component } from 'react';
import { connect } from 'react-redux';
import history from '../utils/history';
import { withRouter } from 'react-router-dom';
import { fetchOpeningQuestions, fetchResponses, submitResults } from '../../actions/index';
import UpdateCompanyInfoForm from './UpdateCompanyInfoForm';




class UpdateSurveyDetails extends Component {

    componentWillMount() {
        this.props.fetchResponses();
        this.props.fetchOpeningQuestions();
    }

    renderTopBanner() {
        return (
            <div className="search-banner single-category">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading">Update My Company Info</h3>
                                <p className="sub-heading">Editing this will update your recommended trials.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleSubmit = (values) => {
        this.props.submitResults(values,history);
        history.push('/custom-selection')

    };

    render() {
        return (
          <div id="wrap">
              { this.renderTopBanner() }
              <div id="main" className="container">

                  <UpdateCompanyInfoForm onSurveySubmit={this.handleSubmit} introQuestions={ this.props.introQuestions } surveyResponses={this.props.surveyResponses} />
              </div>

          </div>
        );
    }
}

function mapStateToProps({ surveyResponses, introQuestions }) {

    return { surveyResponses, introQuestions };
};

export default connect(mapStateToProps,{ fetchOpeningQuestions, fetchResponses, submitResults})(withRouter(UpdateSurveyDetails));