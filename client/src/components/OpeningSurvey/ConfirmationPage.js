/**
 * Created by AnthonyMaina on 2/12/18.
 */

import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';


class ConfirmationPage extends Component {
    render(){
        return(
          <div>
            Thank you!
          </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        formValues: state.form.surveyForm.values
    };
}

export default connect(mapStateToProps)(withRouter(ConfirmationPage));