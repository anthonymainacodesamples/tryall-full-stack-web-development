/**
 * Created by AnthonyMaina on 2/28/18.
 */


import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


class RememberToIndicate extends Component {
    render() {
        return (
            <Modal
                {...this.props}
                bsSize="small"
                aria-labelledby="contained-modal-title-sm"
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm">Gentle Reminder</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>
                        Kindly remember to come back and indicate if you started your free trial for {this.props.title}.
                    </p>
                </Modal.Body>
                <Modal.Footer>
                    <a href={this.props.trialLink} target="_blank">
                        <Button onClick={() => this.props.switchModal()} bsStyle="success" className="center">Proceed To Site</Button>
                    </a>
                </Modal.Footer>
            </Modal>
        );
    }

}
export default RememberToIndicate;