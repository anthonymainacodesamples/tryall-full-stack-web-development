/**
 * Created by AnthonyMaina on 11/30/17.
 */

import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Modal from './modal-shim';
import '../../styles/ModalTextStyling.css';
import {Link} from 'react-router-dom';

//Import Moment for displaying issues
import moment from 'moment';

class SubQuestionModal extends Component {


    getCurrentDate() {
        return(
            moment().format("dddd, MMMM Do YYYY")
        );
    }

    renderFooterLink() {
        return (
            <div className="text-center">
                <a>Yes I did but on a different date.</a>
            </div>
        );
    }


    render() {
        return (
            <Modal {...this.props} aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm" className="text-center" >Quick Question</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center">
                        <p>Did you end up subscribing for the {this.props.title} free trial on {this.getCurrentDate()}?</p>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.props.addToActive()} bsStyle="success" className="pull-left">Yes, I did.</Button>
                    <Link to={`/subfail/${this.props.trialID}`}  style={{ textDecoration: 'none' }}>
                        <Button onClick={() => this.props.clickedDidNotSubscribe()} bsStyle="danger">No, I didn't.</Button>
                    </Link>
                </Modal.Footer>

            </Modal>
        );
    }
}
export default SubQuestionModal;