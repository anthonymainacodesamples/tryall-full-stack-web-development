/**
 * Created by AnthonyMaina on 10/20/17.
 */

import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Modal from './modal-shim';

class SubscribeModal extends Component {

    render() {
        return(
            <Modal {...this.props} aria-labelledby="contained-modal-title-sm">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-sm" className="text-center">Gentle Reminder</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="text-center">
                    <p>Kindly remember to come back and indicate if you started your free trial for {this.props.title}.</p>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <a href={this.props.trialLink} target="_blank">
                        <Button onClick={() => this.props.switchModal()} bsStyle="success" className="center">Proceed To Site</Button>
                    </a>
                </Modal.Footer>

            </Modal>
        );
    }
}

export default SubscribeModal;