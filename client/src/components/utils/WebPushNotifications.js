/**
 * Created by AnthonyMaina on 12/10/17.
 */

const applicationServerPublicKey = 'BJjpqIYKlUcC0w21g8rncHWs-4m7bk6M9T3VrGKyTJ9X2Lr1Jr02Jm1kkPS5WeE8L_G5j9uSa38ms9OYe4bPLec';

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

export default {urlBase64ToUint8Array, applicationServerPublicKey }