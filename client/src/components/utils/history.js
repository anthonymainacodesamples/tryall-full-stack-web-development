/**
 * Created by AnthonyMaina on 2/12/18.
 */


// history.js
import { createBrowserHistory } from 'history'

export default createBrowserHistory({
    /* pass a configuration object here if needed */
})