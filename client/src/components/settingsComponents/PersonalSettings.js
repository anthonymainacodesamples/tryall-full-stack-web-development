/**
 * Created by AnthonyMaina on 10/21/17.
 */
import React,{ Component } from 'react';
import { Glyphicon, Fade} from 'react-bootstrap';
import reminderPeriods from './reminderPeriods';
import _ from 'lodash';
import { fetchSettings,updateReminders, updateFrequency } from '../../actions/index';
import { connect } from 'react-redux';
import NotSubCheckBox from '../miniComponents/NotSubCheckBox';
import '../../styles/SettingsPage.css';

import AlertContainer from 'react-alert'
import SuccessIcon from '../../images/success.png';



class PersonalSettings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open:false,
            disabled: false
        };

        this.selectedCheckboxes = new Set();
        this.activate = this.activate.bind(this);
        this.deactivate = this.deactivate.bind(this);
        this.handleClickUpdate = this.handleClickUpdate.bind(this);
    }

    alertOptions = {
        offset: 14,
        position: 'top right',
        theme: 'light',
        time: 5000,
        transition: 'scale'
    };

    showReminderUpdateAlert = () => {
        this.msg.show('Updated Reminders.', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    showFrequencyUpdateAlert = () => {
        this.msg.show('Frequency Settings Updated', {
            time: 2000,
            type: 'success',
            icon: <img src={SuccessIcon} alt="success" />
        })
    };

    componentDidMount() {
        this.props.fetchSettings();
    }

    toggleCheckbox = label => {
        if (this.selectedCheckboxes.has(label)) {
            this.selectedCheckboxes.delete(label);
        } else {
            this.selectedCheckboxes.add(label);
        }
        this.setState({ open: true });
    };

    handleFormSubmit = formSubmitEvent => {
        formSubmitEvent.preventDefault();



        for (const checkbox of this.selectedCheckboxes) {
            console.log(checkbox, 'is selected.');
            var foundObject = _.find(reminderPeriods, {label: checkbox});
           this.props.updateReminders(foundObject.name,true);

        }

        this.showReminderUpdateAlert()
    };

    // isValueChecked(label) {
    //     const { settings } = this.props;
    //     const { preferences } = settings;
    //     return _.find(preferences, {label: label});
    //     console.log('hi');
    // }

    isValueChecked = ( (label) => {
        const { settings } = this.props;
        const { preferences } = settings;
        _.find(preferences, {label: label});
    });


    createCheckbox = ({ label }) => (
        <div className="col-md-6"  key={label}>
            <NotSubCheckBox
                checkedStatus={true}
                label={label}
                handleCheckboxChange={this.toggleCheckbox}
                key={label}

            />
        </div>
    );

    createCheckboxes = () => (
        reminderPeriods.map(this.createCheckbox)
    );

    renderSettingsHeader() {
        return (
            <div className="page-header-homepage">
                <h3> Account Settings </h3>
            </div>
        );
    }

    handleClickUpdate() {
        this.setState({open: false});

    }


    renderReminders() {
        return(
            <div>
                <h4>When would you like to be reminded about the end of your free trial?</h4>
                <div>
                    {this.renderReminderSelection()}
                </div>
            </div>
        );
    }
    renderReminderSelection() {
        return (
            <form onSubmit={this.handleFormSubmit}>
                {this.createCheckboxes()}
                <div className="col-md-offset-2">
                <Fade in={this.state.open}>
                    <button className="dark-blue-general-button" type="submit" onClick={this.handleClickUpdate}>
                        <Glyphicon glyph="time"/>
                        Update Personal Reminders
                    </button>
                </Fade>
                </div>
            </form>
        );
    }

    deactivate() {
        this.props.updateFrequency(false);
        this.props.fetchSettings();
        this.showFrequencyUpdateAlert();

    }

    activate() {
        this.props.updateFrequency(true);
        this.props.fetchSettings();
        this.showFrequencyUpdateAlert();
    }

    renderButton() {

        const { settings } = this.props;
        const  { highFrequency } = settings;

        if(highFrequency === true) {

            return (
                <button
                    className="dark-blue-general-button"
                    onClick={this.deactivate}
                >
                    Deactivate
                </button>
            );
        } else {
            return (
                <button
                    className="dark-blue-general-button pull-left"
                    onClick={this.activate}
                >
                    Activate
                </button>
            );
        }
    }

    renderSingle() {
        return (
            <div className="col-md-12">
                <div className="row">
                    <div className="col-md-8">
                    Remind me every day after my first reminder until my free trial ends
                    </div>
                </div>
                <div className="col-md-4">
                    {this.renderButton()}
                </div>
            </div>
        );
    }
    renderNewFrequency() {
        return(
            <div>
                <h4>Frequency</h4>
                <div>
                    <div className="row">
                        {this.renderSingle()}
                        </div>
                </div>
            </div>
        );
    }

    renderTopBanner() {
        return (
            <div className="search-banner">
                <div className="search-top">
                    <div className="container">
                        <div className="main-search-wrapper">
                            <div className="sub-search-text-div">
                                <h3 className="heading"> Account Settings</h3>
                                <p className="sub-heading"> Modify your account preferences here.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {

        return(
          <div>
              {this.renderTopBanner()}
              <div className="container">
              <div className="col-md-8 col-md-offset-2">
                  <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />

                {this.renderReminders()}

                {this.renderNewFrequency()}


              </div>
              </div>

          </div>
        );
    }
}
function mapStateToProps({ settings }) {
    return { settings };
}
export default connect(mapStateToProps, { fetchSettings,updateReminders, updateFrequency })(PersonalSettings);