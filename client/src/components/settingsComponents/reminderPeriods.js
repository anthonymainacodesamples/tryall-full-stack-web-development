/**
 * Created by AnthonyMaina on 10/24/17.
 */
export default [
    {label:'3 Days Before', name:'threeDays', value:3},
    {label:'5 Days Before', name:'fiveDays', value:5},
    {label:'1 Week before', name:'sevenDays', value:7},
    {label:'1 Day Before', name:'oneDay',value:1}
];