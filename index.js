/**
 * Created by AnthonyMaina on 9/28/17.
 */

//Const statements
const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./config/keys');
const bodyParser = require('body-parser');
const admin = require("firebase-admin");


// Slack Webbook URL for server errors
process.env["SLACK_WEBHOOK_URL"] = "https://hooks.slack.com/services/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";


//Require files to run
require('./models/User');
require('./models/Trial');
require('./models/SimilarTrials');
require('./models/SearchQueries');
require('./models/SearchPageClicks');
require('./models/ReminderRecord');
require('./models/Category');
require('./models/BroadCategory');
require('./models/UnsubReasons');
require('./models/SurveyResponse');
require('./models/SurveyQuestion');
require('./models/EssentialCategories');
require('./models/Filters');
require('./models/Testers');
require('./models/ActiveEligibility');
require('./models/MatchesBySchedules');
require('./models/ClicksPerTrial');
require('./models/ClicksPerUser');
require('./models/SubscribedByTrial');




//Services
require('./services/passport');
require('./services/logger');
require('./services/search');
require('./middlewares/slack');



/**
 * Initializing Firebase Admin SDK
 */
let serviceAccount = require("./tryall-dev-firebase-adminsdk-d33xt-8ce8eac4ea.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://tryall-dev.firebaseio.com"
});


//Mongoose to mongodb connection
mongoose.connect(keys.mongoURI, { useMongoClient: true });

//mongoose.connect('mongodb://34.228.18.70:27017/tryall');

//Declare express route handlers for oAuth process
const app = express();

//Make middleware available across the app

/** Make body parser middleware allow app wide POST,PUT,PATCH requests
    to make available body as req.body
 */
app.use(bodyParser.json());

app.use(
    cookieSession({
        //30 days
        maxAge: 30*24*60*60*1000,
        keys: [keys.cookieKey]
    })
);
app.use(passport.initialize());
app.use(passport.session());

//Require route files in app running
require('./routes/iosAppRoutes')(app);
require('./routes/authRoutes')(app);
require('./routes/searchRoutes')(app);
require('./routes/featuredRoutes')(app);
require('./routes/trialsRoutes')(app);
require('./routes/userRoutes')(app);
require('./routes/dataCollectionRoutes')(app);
require('./routes/remindersRoutes')(app);
require('./routes/categoriesRoutes')(app);
require('./routes/notificationRoutes')(app);
require('./routes/surveyRoutes')(app);
require('./routes/testerRoutes')(app);
require('./routes/customCategoriesRoutes')(app);
require('./routes/testerRoutes')(app);



process.env['NODE_ENV'] = 'production';
if (process.env.NODE_ENV === 'production') {

    // Express will serve up production assets
    //Like main.js or main.css!
    app.use(express.static('./client/build'));


    //Express will serve up index.html if it doesn't recognize the route
    const path = require('path');
    app.get('*', (req,res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}


//Listening port info
const PORT = process.env.PORT || 8000;

app.listen(PORT);

