const Transport = require('winston-transport');
const util = require('util');
const logger = require('../services/logger');
const IncomingWebhook = require('@slack/client').IncomingWebhook;

const url = process.env.SLACK_WEBHOOK_URL || '';

const webhook = new IncomingWebhook(url);



//
// Inherit from `winston-transport` so you can take advantage
// of the base functionality and `.exceptions.handle()`.
//
module.exports = class SlackLogger extends Transport {

  constructor(opts) {
    super(opts);
    //
    // Consume any custom options here. e.g.:
    // - Connection information for databases
    // - Authentication information for APIs (e.g. loggly, papertrail,
    //   logentries, etc.).
    //
  }

  log(info, callback) {

    // console.log(`we outchea: ${info.level} with message: ${info.message}`);
    if(info.level === "emerg" || info.level === "alert" || info.level === "crit" || info.level === "error" ||
    info.level === "warning" || info.level === "notice"){

      let message = `New server error - Level: ${info.level}, Message: ${info.message}`;
      console.log(message);

      webhook.send(message, function(err, header, statusCode, body) {
        if (err) {
          logger.error('Error:', err);
        } else {
          logger.info('Received', statusCode, 'from Slack');
        }
      });
    }

    // Perform the writing to the remote service
    callback();
  }
};