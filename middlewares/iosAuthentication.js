/**
 * Created by AnthonyMaina on 11/14/17.
 */
const GoogleAuth = require('google-auth-library');
const auth = new GoogleAuth();
const iosClientID = require('../config/keys').iosClientID;
const client = new auth.OAuth2(iosClientID, "","");
const mongoose = require('mongoose');
const User = mongoose.model('users');


module.exports = async (req,res,next) => {
    if(req.query.id_token) {
            client.verifyIdToken(
            req.query.id_token,
            iosClientID,
            async (e, login) =>  {
                if (login) {
                    const payload = await login.getPayload();
                    const userid = payload['sub'];
                    const mongouserID = await User.find({googleId: userid }).select('_id savedTrials activeTrials pastTrials');
                    req.user = mongouserID[0];
                    next();
                } else {
                    return res.status(401).send({error: 'Try logging in to the app again'});
                }
            }
        );
    } else {
        next();
    }
};