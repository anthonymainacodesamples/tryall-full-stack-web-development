var mongoose = require('mongoose');
const Company = mongoose.model('TryallCompany');
const Trial = mongoose.model('tryallcompanies');
const logger = require('../services/logger');
const BroadCategory = mongoose.model('broad-categories');

module.exports = (app) => {

  app.post('/search', function (req, res) {

    let term = req.body.searchTerm;

    let query = {
      "from": 0,
      "size": 15,
      "query": {
        "bool": {
          "must": [
            {
              'query_string':{
                'query': `**${term}*`,
                'fields': ['name^2','categories']
              }
            },
            {
              "wildcard": { // Only returning fields that have a description.
                            // TODO: This is an unoptimized solution
                            // https://github.com/elastic/elasticsearch/issues/7515
                "description": "*"
              }
            }
          ],
          "must_not": [
            {
              "term": { // Only returning fields that do not have 0 as the duration
                "DurationInDays": 0
              }
            },
            {
              "term": {
                "monthlyDollarPrice": 0
              }
            },
            {
              "term": {
                "monthlyDollarPrice": 0.1
              }
            },
            {
              "term": {
                "monthlyDollarPrice": "0"
              }
            },
            {
              "term": {
                "monthlyDollarPrice": "0.1"
              }
            }
          ]

        }
      }
    };

    Company.esSearch(query, function(err, results) {
      if(err){
        logger.error(`Error finding search results: ${err}`);
      }

      let arr = [];

      if(results && results.hits){
        results.hits.hits.forEach(function (item) {
          console.log(item._source);
          arr.push({
            name: item._source.name,
            _id: item._id,
            description: item._source.description,
              logoLink:item._source.logoLink,
              DurationInDays:item._source.DurationInDays,
              monthlyDollarPrice:item._source.monthlyDollarPrice
          });
        });
      }

      res.status(200).send(arr);

    });

  });

  app.post('/search-categories', async  (req, res) => {

      let broadCategories = await BroadCategory.find((err, res) => {
          if(err) logger.error(`error getting broad categories in searchRoutes: ${err}`);
          else{
              broadCategories = res;
          }
      });


    let term = req.body.searchTerm;

    console.log('Search Categories value from frontend', term);

    let toSearch = [];

    broadCategories.forEach(item => {
      console.log(item);
      if(item.name.toLowerCase() === term.toLowerCase()){
        if(item.sub_categories.length !== 0){
          item.sub_categories.forEach(sub => {
            toSearch.push(sub)
          })
        }
      }
    });

    // console.log(`to search: ${toSearch}`);

    Trial.find(
      {
        categories: {
        $in: toSearch
      },
        DurationInDays: {
          $ne: 0
        },
        monthlyDollarPrice: {
          $nin: [0, 0.1, "0", "0.1"]
        }
      },
      (err, results) => {
        if(err){
          logger.error(`error finding results for categories: ${err}`);
        }else{
          let arr = [];
          results.forEach(item => {
            arr.push(item)
          });
          res.send(arr)
        }
     });
    /*let matches = [];

    toSearch.forEach(item => {
      let x = {
        match: {
          categories: item
        }
      };
      matches.push(x);
    });

    let wildCard = {
      "wildcard": { // Only returning fields that have a description.
        "description": "*"
      }
    };

    let query = {
      "query": {
        "bool": {
          "should": matches,
          'must_not': [
            {
              "term": {
                "DurationInDays": 0
              }
            },
            {
              "term": {
                "monthlyDollarPrice": 0
              }
            },
            {
              "term": {
                "monthlyDollarPrice": 0.1
              }
            },
            {
              "term": {
                "monthlyDollarPrice": "0"
              }
            },
            {
              "term": {
                "monthlyDollarPrice": "0.1"
              }
            }
          ]
        }
      }
    };*/

    // console.log(JSON.stringify(query, null, 2));

   /* Company.esSearch(query, function(err, results) {
      if(err){
        logger.error(`Error finding search results for categories: ${err}`);
      }

      let arr = [];

      if(results && results.hits){
        results.hits.hits.forEach(function (item) {
          console.log(`name: ${item._source.name}, categories: - ${item._source.categories}`);
          arr.push({
            name: item._source.name,
            _id: item._id,
            description: item._source.description,
            logoLink:item._source.logoLink,
            DurationInDays:item._source.DurationInDays,
            monthlyDollarPrice:item._source.monthlyDollarPrice
          });
        });
        // console.log(results.hits.hits);
      }

       console.log(`results length: ${arr.length}`);
      res.status(200).send(arr);

    });*/

  });

};

