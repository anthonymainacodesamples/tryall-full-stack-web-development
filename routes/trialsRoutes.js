/**
 * Created by AnthonyMaina on 10/17/17.
 */

const mongoose = require('mongoose');
const logger = require('../services/logger');
const requireLogin = require('../middlewares/requireLogin');
const iosAuthentication = require('../middlewares/iosAuthentication');
const _ = require('lodash');
const User = mongoose.model('users');
const ActiveEligibility = mongoose.model('active_eligibility_items');
const ScheduleConversation = mongoose.model('match_schedule_records');
const Trial = mongoose.model('tryallcompanies');
const Similar = mongoose.model('similarcompanies');

const ONE_DAY = 1000 * 60 * 60 * 24;

let recommendedTrials15 = [];
let recommendedTrials30 = [];


getValidityPeriodStatus = (date, daysToAdd) => {
    let result = new Date(date);
    result.setDate(result.getDate() + daysToAdd);
    return result;
};


module.exports = (app) => {

    app.get('/api/trials/recommended', async (req,res) => {


      //Summarized Boolean value to determine if it needs 15 or 30
      let summarizedBooleanValue = req.query.summarizedVal;
      let bool = summarizedBooleanValue === 'true';

      if(bool){
        if(req.session.dateLastRequested15Recommended){
          // console.log(`date last requested 15: ${req.session.dateLastRequested15Recommended}`);

          if((new Date()  - req.session.dateLastRequested15Recommended) < ONE_DAY){
            return recommendedTrials15
          }
        }else{
          req.session.dateLastRequested15Recommended = new Date();
        }
      }else{
        if(req.session.dateLastRequested30Recommended){
          // console.log(`date last requested 30: ${req.session.dateLastRequested30Recommended}`);

          if((new Date() - req.session.dateLastRequested30Recommended) < ONE_DAY){
            return recommendedTrials30
          }
        }else{
          req.session.dateLastRequested30Recommended = new Date();
        }
      }

        let userID = req.user._id;
        const followedCategories = await User.findOne({_id: userID}).populate("followedCategories").select("followedCategories");

        // console.log(`followed categories: ${followedCategories.followedCategories}`);

      // Without 2nd "followedCategories, results would come back with an id field
      if(followedCategories.followedCategories.length === 0){
        try {
          let trials =  Trial.aggregate(
            [
              {
                $match: {
                  tagLine: {
                    $ne: ""
                  },
                  description: {
                    $ne: "",
                    $exists: true
                  },
                  monthlyDollarPrice: {
                    $nin: [0, 0.1, "0", "0.1"]
                  },
                  DurationInDays: {
                    $ne: 0
                  }
                }
              },
              {
                $sample: {
                  size: bool ? 15 : 30
                }
              }
            ]);

          if(bool){
            recommendedTrials15 = trials
          }else{
            recommendedTrials30 = trials
          }
          return trials;
        } catch (err) {
          logger.error(`error getting random trials for recommended: ${err}`);
        }
      }else{

        let matches = [];

        Array.prototype.forEach.call(followedCategories.followedCategories, item => {
          // console.log(`item: ${item}`);
          matches.push(item.name)
        });
        // console.log(`matches: ${matches}`);

        Trial.find(
          {
            categories: {
              $in: matches
            },
            DurationInDays: {
              $ne: 0
            },
            monthlyDollarPrice: {
              $nin: [0, 0.1, "0", "0.1"]
            }
          },
          (err, results) => {
            if(err){
              logger.error(`error finding results for recommended trials: ${err}`);
            }else{
              // console.log(results);
              let arr = [];
              results.forEach(item => {
                arr.push(item)
              });

              if(bool){
                recommendedTrials15 = arr
              }else{
                recommendedTrials30 = arr
              }
              res.send(arr)
            }
          }).limit(bool ? 15 : 30);
      }
    });

    app.get('/api/trials/active',iosAuthentication, async (req, res) => {
        const trialIds = req.user.activeTrials;
        const userId = req.user._id;

        //Get Tester Ids
        const trialsAsTester = await ScheduleConversation.find({testerId:userId });
        const trialsTesterIds = _.map(trialsAsTester, ({trialId}) => {
            return trialId;
        });
        //Get Pending Trial Ids from existing match schedule records.
        const trialsPending = await ScheduleConversation.find({userId, confirmedTimeStatus: false});
        const pendingTrialsIds = _.map(trialsPending, ({ trialId }) => {
            return trialId;
        });

        const trials = await Trial.find({_id:
            {
                $in:trialIds
            }
        });
        try {
            res.send({trials,trialsTesterIds, pendingTrialsIds });
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.get('/api/trials/saved',iosAuthentication,async (req, res) => {
        const trialIds = req.user.savedTrials;
        const trials = await Trial.find({_id:
            {
                $in:trialIds
            }
        });
       try {
           res.send(trials);
       }catch(err) {
           res.status(404).send(err);
       }
    });
    app.get('/api/trials/past',iosAuthentication,async (req, res) => {
        const trialIds = req.user.pastTrials;
        const trials = await Trial.find({_id:
            {
                $in:trialIds
            }
        });
        try {
            res.send(trials);
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.get('/api/trials/:id', async (req,res) => {
        const idExtracted =  req.params.id;
        const trial = await Trial.find({_id:idExtracted});
        try {
            res.send(trial);
        }catch(err) {
            res.status(404).send(err);
        }
    });



    app.post('/api/trials/saved/:id',iosAuthentication, requireLogin, (req,res) => {

        let currentId = req.params.id;
        User.findByIdAndUpdate(req.user.id,
            {
                $addToSet:{savedTrials:currentId}
            }
        ).exec();
        res.send({});
    });

  app.delete('/api/trials/saved/:id',iosAuthentication, requireLogin,(req,res) => {
      const idExtracted =  req.params.id;
      User.findByIdAndUpdate(req.user.id,
          {
            $pull:{savedTrials:idExtracted}
          }
      ).exec();
      res.send({});

  });


    app.post('/api/trials/active',requireLogin, async (req,res) => {
        let currentId = req.body.trialId;
        let dateSubscribed = req.body.dateSubscribed;
        let duration = req.body.duration;
        let userID = req.user._id;

        let trialObject = {
            trialId:currentId,
            dateSubscribed,
            duration
        };

        User.updateOne({
                _id:userID
            },
            {
                $addToSet:{activeTrials:currentId}
            }
        ).exec();


        let conditions = {
            userId: userID,
            'trialsInfo.trialId': { $ne: currentId }
        };
        let update = {
            $addToSet: { trialsInfo: trialObject }
        };



        let itExists = await ActiveEligibility.find({userId: userID});

        try {
        if(itExists.length > 0) {
            ActiveEligibility.findOneAndUpdate(conditions,update).exec();
        } else {

                ActiveEligibility.findOneAndUpdate(conditions,update,{ upsert: true, new: true, setDefaultsOnInsert: true } ).exec();
            }
        } catch(err) {
            console.log(err);
        }

        res.send({});
    });

    app.get('/api/similar/:id',requireLogin, async (req, res) => {
        try {
            const tryallId = req.params.id;
            const SimilarTrials = await Similar.find({_id:tryallId}).select('-_id similar_tryalls');
            const trialsObjectArray = SimilarTrials[0].similar_tryalls;
            const arrayIds = trialsObjectArray.map((a) =>  (a.id));

            // TODO: Removing extra and parameters except for "_id" returned "Planyp" for all similar trials
            const finalTrials  = await Trial.find({
                $and: [
                  {
                    _id: {
                      $in:arrayIds
                    }
                  },
                  {
                    monthlyDollarPrice: {
                      $nin: [0, 0.1]
                    }
                  },
                  {
                    DurationInDays: {
                      $ne: 0
                    }
                  }
                ]
            });
            res.send(finalTrials);
        } catch (err) {
            res.status(404).send(err);
        }
  });
};
