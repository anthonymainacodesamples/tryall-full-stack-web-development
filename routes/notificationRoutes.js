/**
 * Created by AnthonyMaina on 12/10/17.
 */


const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');
const User = mongoose.model('users');
const keys = require('../config/keys');

const webpush = require('web-push');

const vapidKeys = {
    publicKey: keys.notifPublicKey,
    privateKey: keys.notifPrivateKey
};


module.exports = (app) => {

    app.post('/api/save-subscription', (req,res) => {

        const interimSubObject = req.body.subscriptionObj;
        const subObject = JSON.parse(interimSubObject);

        const isValidSaveRequest = (req, res) => {
            // Check the request body has at least an endpoint.
            if (!interimSubObject || !subObject.endpoint) {
                // Not a valid subscription.
                res.status(400);
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({
                    error: {
                        id: 'no-endpoint',
                        message: 'Subscription must have an endpoint.'
                    }
                }));
                return false;
            }
            return true;
        };


            if (isValidSaveRequest(req, res)) {
                try {
                    let userID = req.user._id;
                    User.findByIdAndUpdate(userID,
                        {
                            webPushSubscription: subObject
                        }
                    ).exec();
                    res.status(200).send({});
                } catch (err) {
                    res.status(404).send(err);
                }
        }
    });

    app.get('/api/notification-state', async (req,res) => {
        try {
            const userID = req.user._id;
            const notification = await User.findOne({_id: userID}).select("-_id webPushSubscription");
            res.status(200).send(notification);
        } catch(err) {
            res.status(404).send(err);
        }

    });

    app.post('/api/trigger-push-msg/',async  (req,res) => {

        // const userID = '5a2a375ab83cff21896da5bb';
        //
        // return webpush.sendNotification(subscription, dataToSend)
        //     .catch((err) => {
        //         if (err.statusCode === 410) {
        //             return deleteSubscriptionFromDatabase(subscription._id);
        //         } else {
        //             console.log('Subscription is no longer valid: ', err);
        //         }
        //     });


        // const triggerPushMsg = function(subscription, dataToSend) {
        //     return webpush.sendNotification(subscription, dataToSend)
        //         .catch((err) => {
        //             if (err.statusCode === 410) {
        //                 return deleteSubscriptionFromDatabase(subscription._id);
        //             } else {
        //                 console.log('Subscription is no longer valid: ', err);
        //             }
        //         });
        // };
        //
        //
        // function getSubscriptionFromDb(id) {
        //     const frequency = User.find({
        //         _id:userID,
        //     }).select(' -_id webPushSubscription');
        //
        // }
        //
        // function deleteSubscriptionFromDatabase(id) {
        //
        //
        // }
    });


    app.get('/api/test-push', async (req,res) => {
        const userID = '5a2a375ab83cff21896da5bb';

        const subscriptionObj = await User.findOne({_id: userID}).select("-_id webPushSubscription");
        const subscription = subscriptionObj.webPushSubscription;

        const dataToSend = "Netflix";
        webpush.setVapidDetails(
            'mailto:hello@tryall.co',
            vapidKeys.publicKey,
            vapidKeys.privateKey
        );

        // await webpush.sendNotification(subscription, dataToSend);
        // res.send({});

        if(subscription) {

            console.log('Data To send', dataToSend);
            console.log("Subscription object", subscription);

            return webpush.sendNotification(subscription, dataToSend)
                .catch((err) => {
                    if (err.statusCode === 410) {
                        return deleteSubscriptionFromDatabase(subscription._id);
                    } else {
                        console.log('Subscription is no longer valid: ', err);
                    }
                });
        }


        // const triggerPushMsg = function(subscription, dataToSend) {
        //     return webpush.sendNotification(subscription, dataToSend)
        //         .catch((err) => {
        //             if (err.statusCode === 410) {
        //                 return deleteSubscriptionFromDatabase(subscription._id);
        //             } else {
        //                 console.log('Subscription is no longer valid: ', err);
        //             }
        //         });
        // };
        //
        //
        // function getSubscriptionFromDb(id) {
        //     const frequency = User.find({
        //         _id:userID,
        //     }).select(' -_id webPushSubscription');
        //
        // }

        function deleteSubscriptionFromDatabase(id) {
        console.log("Deleted from db");

        }

    });

};