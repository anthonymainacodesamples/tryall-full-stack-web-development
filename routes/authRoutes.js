/**
 * Created by AnthonyMaina on 9/28/17.
 */
const passport = require('passport');

module.exports = (app) =>
{
    app.get('/auth/google', passport.authenticate('google', {
            scope: ['profile', 'email']
        })
    );

    app.get(
        '/auth/google/callback',
        passport.authenticate('google'),
        (req, res) => {
            // const categories = req.user.followedCategories;
            const filledSurvey = req.user.surveyResponded;

            //Code to check for categories
            // if(categories.length > 0 ) {
            //     res.redirect('/homefeed');
            // } else {
            //     res.redirect('/categories');
            // }
            if(filledSurvey) {
                res.redirect('/homefeed');
            } else {
                res.redirect('/info-survey');
            }
        }
    );

    app.get('/api/logout', (req,res) => {
        req.logout();
        res.redirect('/');

    });

    app.get('/api/current_user', (req, res) => {
        res.send(req.user);
    });
};
