/**
 * Created by AnthonyMaina on 2/11/18.
 */


const mongoose = require('mongoose');
const SurveyResponse = mongoose.model('onboarding_responses');
const Question = mongoose.model('onboarding_questions');
const User = mongoose.model('users');
const _ = require('lodash');

module.exports = (app) => {

    app.get('/api/intro/prompts', async (req, res) => {

        const questions = await Question.find({});

        try {
            res.send(questions);
        }catch(err) {
            res.status(404).send(err);
        }

    });

    app.get('/api/responses/survey', async (req,res) => {


        const userId = req.user._id;

        const responses = await SurveyResponse.find({userId}).select('-_id -userId');

        const responseValues = responses[0].responses;

        let responseObj = {};
        _.forEach(responseValues, ({ questionId, responseId }) => {

            responseObj[`${questionId}`] = responseId

        });

        try {
            res.send(responseObj);
        }catch(err) {
            res.status(404).send(err);
        }

    });

    app.post('/api/intro/survey', async (req,res) => {
        let userId = req.user._id;

        const responses = req.body;
        const interimArr = [];


        const keys = Object.keys(responses);
        keys.forEach((key) =>  {
            let value = responses[key];
            interimArr.push({"questionId":key,"responseId":value })
        });


        try {
            User.findByIdAndUpdate(userId,
                {
                    surveyResponded:true
                }
            ).exec();
            SurveyResponse.findOneAndUpdate({ userId },
                {
                    userId,
                    responses:interimArr
                },
                {
                    upsert: true,
                    new: true
                }
            ).exec();
            res.send({});
        } catch (err) {
            res.status(404).send(err);
        }
    });



};