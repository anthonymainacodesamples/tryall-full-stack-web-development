/**
 * Created by AnthonyMaina on 10/29/17.
 */

const mongoose = require('mongoose');
const SearchPageClicks = mongoose.model('search_page_clicks_data');
const UnsubSurveyReasons = mongoose.model('unsub_reasons');
const ClicksPerTrial = mongoose.model('clicks_per_trials');
const ClicksPerUser = mongoose.model('clicks_per_users');
const SearchQueries = mongoose.model('search_query_data');


module.exports = (app) =>
{
    app.get('/api/data/search_clicks', (req,res) => {

    });

    app.post('/api/data/search_queries',async (req,res) => {
        const searchQuery = req.body.searchQuery;
        const userId = req.user._id;

        const search_query = new SearchQueries({
            userId,
            searchQuery,
            dateTimeSearched:Date.now()
        });
        try {
            await search_query.save();
            res.send({});

        } catch(err) {
            res.status(422).send(err);
        }

    });

    app.post('/api/data/unsub_reasons', async (req, res) => {
        const reasonProvided = req.body.unsubReason;
        const dateForReason = Date.now();
        const trialId = req.body.trialID;
        const userId = req.user._id;


        const unsub_reason = new UnsubSurveyReasons({
            userId,
            trialId,
            dateForReason,
            reasonProvided
        });

        try {
            await unsub_reason.save();
            res.send({});

        } catch(err) {
            res.status(422).send(err);
        }


    });

    app.post('/api/data/click-record', async (req,res) => {
       const userId = req.user._id;
       const trialId = req.body.trialId;
       const dateClicked = Date.now();

       try {

           //Save to each trial table
           ClicksPerTrial.findOneAndUpdate({trialId},
               {
                   $addToSet:{
                       clickedUsersInfo:{
                           userId,
                           dateClicked

                       }
                   }
               },
               {
                   upsert: true,
                   new: true
               }
           ).exec();

           //Save to each user table
           ClicksPerUser.findOneAndUpdate({userId},
               {
                   $addToSet:{
                       clickedTrials:{
                           trialId,
                           dateClicked

                       }
                   }
               },
               {
                   upsert: true,
                   new: true
               }
           ).exec();
           res.send({});

       }catch(err) {
           res.status(422).send(err);
       }


    });
};