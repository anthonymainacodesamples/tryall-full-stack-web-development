/**
 * Created by AnthonyMaina on 2/13/18.
 */


const mongoose = require('mongoose');
const BroadCategory = mongoose.model('broad-categories');
const User = mongoose.model('users');
const Trial = mongoose.model('tryallcompanies');
const Filters = mongoose.model('filters');
const SurveyResponses = mongoose.model('onboarding_responses');
const SurveyQuestions = mongoose.model('onboarding_questions');
const _ = require('lodash');
const EssentialCategory = mongoose.model('essential_categories');

module.exports = (app) => {

    app.get('/api/custom/categories', async (req,res) => {
        let userId = req.user._id;

        //Retrieve Personal Filters
        let personalResponseObject = await SurveyResponses.find({ userId });
        let responsesArray = personalResponseObject[0].responses;
        let filtersIdValid = [];
        let filtersProcessed = 0;
        _.forEach(responsesArray, async ({ questionId, responseId }) => {
            let interimRes = await SurveyQuestions.find({
                '_id': questionId
            }).select('options');

            let filtersArray = interimRes[0].options;
            let item = _.find(filtersArray,{_id: responseId });
            let linkedFiltersArray = item.linkedFilters;

            _.forEach(linkedFiltersArray, (item) => {
                if(!(_.includes(filtersIdValid,item))) {
                    filtersIdValid.push(item);
                }
            });
            filtersProcessed++;
            // Complete running of async function
                if(filtersProcessed === responsesArray.length) {
                    fetchFilters(filtersIdValid);
                }
        });

         const fetchFilters = async(finalFilterId) => {
             let finalTextFiltersArray = [];
             const filtersText = await Filters.find({_id:
                 {
                     $in:finalFilterId
                 }
             });

             let filtersTextFinalNum = 0;

             _.forEach(filtersText,({ name }) => {
                finalTextFiltersArray.push(name);
                filtersTextFinalNum++;
                 if(filtersTextFinalNum === filtersText.length) {
                     fetchFilteredTrials(finalTextFiltersArray);
                 }

             });


         };

         const fetchFilteredTrials = async (finalTextFiltersArray) => {

        let essentialArray = await EssentialCategory.find({});

        //Adding code for personalized categories as well
        // let categories = await User.findOne({"_id": userId}).populate("followedCategories").exec();
        // let followedCategoriesArray = categories.followedCategories;
        //
        // _.forEach(followedCategoriesArray, (item) => {
        //     if(!(_.some(essentialArray, {categoryId :item._id} ))) {
        //         essentialArray.push(item);
        //     }
        //
        // });

        let broadCategories = await BroadCategory.find((err, res) => {
            if(err) logger.error(`error getting broad categories in personalized routes: ${err}`);
            else{
                broadCategories = res;
            }
        });


        let finalArr = [];

        let itemsProcessed = 0;
        _.forEach(essentialArray,async ({ name }) => {

        let toSearch = [];

        broadCategories.forEach(item => {
            if(item.name.toLowerCase() === name.toLowerCase()){
                if(item.sub_categories.length !== 0){
                    item.sub_categories.forEach(sub => {
                        toSearch.push(sub)
                    })
                }
            }
        });
        let interimObj = {};
        await Trial.find(
            {
                categories: {
                    $in: toSearch
                },
                DurationInDays: {
                    $ne: 0
                },
                monthlyDollarPrice: {
                    $nin: [0, 0.1, "0", "0.1"]
                }
            },
            (err, results) => {
                if(err){
                    logger.error(`error finding results for categories: ${err}`);
                }else{
                    let arr = [];
                    results.forEach(item => {
                        let found = item.filters.some(item => finalTextFiltersArray.includes(item));
                        if(found) {
                            arr.push(item);
                        }
                    });
                    interimObj = {
                        categoryName: name,
                        trialsList:arr
                    };

                }
            });

            itemsProcessed++;
            finalArr.push(interimObj);

            //Complete running of async function
            if(itemsProcessed === essentialArray.length) {
                callback(finalArr);
            }
        });
         };

        function callback (finalArr) {
            try {
                res.send(finalArr);
            } catch (err) {
                res.status(404).send(err);
            }
        }
    });

};
