/**
 * Created by AnthonyMaina on 10/30/17.
 */

const mongoose = require('mongoose');
const Reminder  = mongoose.model('reminder_records');
const Trial = mongoose.model('tryallcompanies');
const User = mongoose.model('users');

getReminderDate = (date, daysToAdd) => {
  let result = new Date(date);
  result.setDate(result.getDate() + daysToAdd);
  return result;
};

getEndDate = (date, daysToAdd) => {
  let result = new Date(date);
  result.setDate(result.getDate() + daysToAdd);
  return result;
};

updateActiveTrials = async (userId, trialId, dateSubscribed, duration, daysToSubtract) => {

  let reminderDate = getReminderDate(dateSubscribed, duration - daysToSubtract);
  let endDate = getEndDate(dateSubscribed, duration);
  console.log(`reminder date: ${reminderDate}`);
  console.log(`end date: ${endDate}`);

  const userRecord = await Reminder.findOne({userId});
  if (userRecord) {
    Reminder.findOneAndUpdate(
      {userId},
      {
        $push: {
          "activeTrials": {
            trialId,
            dateSubscribed,
            reminderDate,
            endDate
          }
        }
      }
    ).exec();
    return ({});
  }

  const newReminder = new Reminder({
    userId,
    activeTrials: {
      trialId,
      dateSubscribed,
      reminderDate,
      endDate
    }
  });

  try {
    await newReminder.save();
    return true;
  } catch (err) {
    return false;
  }
};

module.exports = (app) => {
    app.post('/api/reminder/save', async (req,res) => {

        const userId = req.user._id;
        const { trialId, dateSubscribed } = req.body;

        const trialRecord = await Trial.findOne({'_id': trialId});
        console.log(`duration in days: ${trialRecord.DurationInDays}`);

        //TODO: "Days to add" should be retrieved from user settings

        let user = await User.findOne({"_id": userId});
        let prefs = user.preferences;
        console.log(`prefs: ${prefs}`);

        let daysToSubtract = 0;

        let sevenDays = prefs.sevenDays;
        let fiveDays = prefs.fiveDays;
        let threeDays = prefs.threeDays;
        let oneDay = prefs.oneDay;

        let resSeven, resFive, resThree, resOne;


        // Allowing user to get reminders since user can select multiple options
        if(sevenDays){
            daysToSubtract = 7;
          resSeven = updateActiveTrials(userId, trialId, dateSubscribed, trialRecord.DurationInDays, daysToSubtract)
        }
        if(fiveDays){
            daysToSubtract = 5;
          resFive = updateActiveTrials(userId, trialId, dateSubscribed, trialRecord.DurationInDays, daysToSubtract)
        }
        if(threeDays){
            daysToSubtract = 3;
          resThree = updateActiveTrials(userId, trialId, dateSubscribed, trialRecord.DurationInDays, daysToSubtract)
        }
        if(oneDay){
            daysToSubtract = 1;
          resOne = updateActiveTrials(userId, trialId, dateSubscribed, trialRecord.DurationInDays, daysToSubtract)
        }

        if(!resSeven || !resFive || !resThree || !resOne){
          res.status(422).send("Error saving reminders");
        }

      res.send({});

    });
};