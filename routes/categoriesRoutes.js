const mongoose = require('mongoose');
const BroadCategory = mongoose.model('broad-categories');
const User = mongoose.model('users');
const _ = require('lodash');
const iosAuthentication = require('../middlewares/iosAuthentication');


module.exports = (app) => {


    app.get('/api/categories/:id', async (req,res) => {
        const idExtracted =  req.params.id;
        const broadCat = await BroadCategory.find({_id:idExtracted});

        try {
            res.send(broadCat);
        }catch(err) {
            res.status(404).send(err);
        }

    });

  app.post('/api/categories/:id', async (req,res) => {
      let currentId = req.params.id;
      User.findByIdAndUpdate(req.user.id,
          {
              $push:{followedCategories:currentId}
          }
      ).exec();
      res.send({});
  });
  app.delete('/api/categories/:id', iosAuthentication, async (req,res) => {
        const idExtracted =  req.params.id;
        User.findByIdAndUpdate(req.user.id,
            {
                $pull:{followedCategories:idExtracted}
            }
        ).exec();
        res.send({});
    });

    app.get('/api/categories', async (req, res) => {
        let broadCategories = await BroadCategory.find({});
        let sortedCategories = _.sortBy(broadCategories, ({ name }) => {
            return name;
        });
        try {
            res.send(sortedCategories);
        }catch(err) {
            res.status(404).send(err);
        }
    });


  /**
   * Getting broad categories that user selected
   */
  app.get('/api/my-categories', iosAuthentication, async (req, res) => {
    let userId = req.user.id;
    let categories = await User.findOne({"_id": userId}).populate("followedCategories").exec();
    if(categories){
      res.send(categories.followedCategories)
    }else{
      res.send([])
    }
  });

  /**
   * Add categories to user's selections
   */
  app.post('/api/add-category/:id', iosAuthentication, async (req, res) => {
    let currentId = req.params.id;
    User.findByIdAndUpdate(req.user.id,
      {
        $push:{followedCategories:currentId}
      }
    ).exec();
    res.send({});
  });


  /**
   * Update categories for a user
   * Categories are passed as parameters and is used to replace all user categories currently saved
   */
  app.post('/api/update-categories', iosAuthentication, async (req, res) => {
    let categories = req.body.categories;

    User.findOneAndUpdate(req.user.id,
      {
        $set:{followedCategories:categories}
      }
    ).exec();
    res.send({});
  });
};