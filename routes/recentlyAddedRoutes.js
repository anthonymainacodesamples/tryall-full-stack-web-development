const mongoose = require('mongoose');
const Trial = mongoose.model('tryallcompanies');

module.exports = (app) =>  {
  app.get('/api/recently-added', async (req, res) => {

    const result = await Trial.find().limit(10);
    res.status(200).send(result);
  })

};