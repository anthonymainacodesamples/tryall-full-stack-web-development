/**
 * Created by AnthonyMaina on 10/24/17.
 */
const mongoose = require('mongoose');
const requireLogin = require('../middlewares/requireLogin');
const iosAuthentication = require('../middlewares/iosAuthentication');
const _ = require('lodash');
const User = mongoose.model('users');


module.exports = (app) => {
    app.get('/api/profile/settings',requireLogin, async (req,res) => {
        let userID = req.user._id;
        const settings = await User.find({
            _id:userID,
        }).select(' -_id preferences highFrequency');
        try {
            res.send(settings);
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.post('/api/profile/:token', iosAuthentication, requireLogin,(req,res) => {
        let userID = req.user._id;
        let deviceToken = req.params.token;

        User.findByIdAndUpdate(userID,
            {
              registrationToken: deviceToken
            }
        ).exec();
        res.send({});

    });
    app.post('/api/profile/frequency/:frequency', iosAuthentication, requireLogin,(req,res) => {
        let userID = req.user._id;
        let highFrequency = req.params.frequency;

        User.findByIdAndUpdate(userID,
            {
                highFrequency
            }
        ).exec();
        res.send({});

    });
    app.get('/api/profile/frequency', iosAuthentication, requireLogin,async (req,res) => {
        let userID = req.user._id;
        const frequency = await User.find({
            _id:userID,
        }).select(' -_id highFrequency');
        try {
            res.send(frequency);
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.get('/api/profile/reminders',iosAuthentication, requireLogin, async (req,res) => {
        let userID = req.user._id;
        const sevenDayVal = await User.find({
            _id:userID,
        }).select(' -_id preferences');
        try {
            res.send(sevenDayVal);
        }catch(err) {
            res.status(404).send(err);
        }
    });
    app.post('/api/profile/reminders/sevenDays/:val',iosAuthentication, requireLogin, (req,res) => {
        let userID = req.user._id;
        let updateValue = req.params.val;
        User.findByIdAndUpdate(userID,
            {
                $set:{"preferences.sevenDays": updateValue}
            }
        ).exec();
        res.send({});
    });
    app.post('/api/profile/reminders/fiveDays/:val',iosAuthentication, requireLogin, (req,res) => {
        let userID = req.user._id;
        let updateValue = req.params.val;
        User.findByIdAndUpdate(userID,
            {
                $set:{"preferences.fiveDays": updateValue}
            }
        ).exec();
        res.send({});
    });
    app.post('/api/profile/reminders/threeDays/:val',iosAuthentication, requireLogin, (req,res) => {
        let userID = req.user._id;
        let updateValue = req.params.val;
        User.findByIdAndUpdate(userID,
            {
                $set:{"preferences.threeDays": updateValue }
            }
        ).exec();
        res.send({});
    });
    app.post('/api/profile/reminders/oneDay/:val',iosAuthentication, requireLogin, (req,res) => {
        let userID = req.user._id;
        let updateValue = req.params.val;
        User.findByIdAndUpdate(userID,
            {
                $set:{"preferences.oneDay": updateValue}
            }
        ).exec();
        res.send({});
    });


};