/**
 * Created by AnthonyMaina on 2/20/18.
 */

const mongoose = require('mongoose');
const Trial = mongoose.model('tryallcompanies');
const OnboardingResponses = mongoose.model('onboarding_responses');
const Test = mongoose.model('testers-list');
const ScheduleConversation = mongoose.model('match_schedule_records');
const User = mongoose.model('users');
const _ = require('lodash');
const axios = require('axios');
const moment = require('moment');
const keys = require('../config/keys');
const Mailer = require('../services/Mailer');
const matchTemplate = require('../services/emailTemplates/matchFoundTemplate');


module.exports = (app) => {

    app.get('/api/tester/:id', async (req,res) => {

        const userId = req.user._id;
        const userName = req.user.displayName;
        const trialId = req.params.id;

        try {
            User.findByIdAndUpdate(userId,
                {
                    $addToSet:
                        {
                            testingTrials:trialId
                        }
                }).exec();
            Test.findOneAndUpdate({ trialId },
                {
                    $addToSet:
                        {
                            testers:
                                {
                                    userId,
                                    userName
                                }
                        }
                },
                {
                    upsert: true,
                    new: true
                }).exec();
            res.send({});
    } catch (err) {
            res.status(404).send(err);
    }

    });

    app.get('/api/testing/list',async (req,res) => {

        const trialIds = req.user.testingTrials;
        const trials = await Trial.find({_id:
            {
                $in:trialIds
            }
        });
        try {
            res.send(trials);
        }catch(err) {
            res.status(404).send(err);
        }
    });

    app.get('/api/tester/connect/:id', async (req,res) => {
        const trialId = req.params.id;
        const userId = req.user._id;
        const matchingApi = keys.matchingApiURL;
        try {
            const matchResult = await axios.get(`${matchingApi}/get-match-data/${trialId}/${userId}`);
            const testerId = matchResult.data.Tester['0'];
            if(testerId) {
                    await new ScheduleConversation ({
                    userId,
                    testerId,
                    trialId,
                    conversationComplete:false,
                }).save();
            }
            const currentUserInfo = {
                currentId:req.user._id,
                currentName:req.user.displayName
            };
            const testersObj =  await Test.find({ trialId });
            const testersArray = testersObj[0].testers;
            const singleTester = _.find(testersArray,{"userId" :mongoose.Types.ObjectId(testerId)});
            res.send({connectInfo: {singleTester,currentUserInfo }});
        }catch(err) {
            res.status(404).send(err.response);
        }
    });

    app.post('/api/tester/reject-match',async (req,res) => {
        let userId = req.user._id;
        let trialId = req.body.trialId;
        let testerId = req.body.testerId;
        //Save object to match
        ScheduleConversation.findOneAndUpdate ({
            userId,
            testerId,
            trialId
        },{
            matchRejected:true,
            conversationComplete:true
        }).exec();
        try {
            res.send({})
        } catch(err) {
            res.status(422).send(err);
        }

    });

    app.post('/api/tester/match', async (req,res) => {

        let userId = req.user._id;
        let username = req.user.displayName;
        let trialId = req.body.trialId;
        let testerId = req.body.testerId;
        let timesObj  = req.body.optionsAvailable;
        try {
            ScheduleConversation.findOneAndUpdate({
                userId,
                testerId,
                trialId
            },{
                confirmedTimeStatus:false,
                emailSent:true,
                matchRejected:false,
                conversationComplete:false,
                suggestedOptions:timesObj
            }).exec();

        //Retrieve email of tester
        const testerContact = await User.find({_id:testerId});
        let email = testerContact[0].emailAddress;

        //Send email to tester
        const matchObject = {
            title: 'Speak to a fellow Tryaller.',
            subject:`Tryall needs you`,
            body:`${username} has selected the following 3 times at which they are available to discuss one of the free trials you are currently testing. 
            Click on the time that works for you and remember to log on to Tryall when the time comes.
            `,
            options:timesObj,
            additionalInfo:{
                testerId,
                userId,
                trialId
            },
            recipients: [
                {
                    email
                }
            ]
        };
        //Dispatch Email Task
        const mailer = new Mailer(matchObject, matchTemplate(matchObject));
        await mailer.send();
        res.send({});

        } catch(err) {
            res.status(422).send(err);
        }
    });

    app.post('/api/tester/complete-convo', async (req,res) => {
        const trialId = req.body.trialId;
        const testerId = req.body.testerId;
        const userId = req.user._id;

        try {

            ScheduleConversation.findOneAndUpdate({userId, trialId, testerId},
                {
                    conversationComplete: true
                }).exec();
        }catch(err) {
            res.status(422).send(err);
        }

    });

    app.post('/api/tester/confirm/match', async (req,res) => {
        const trialId = req.body.trialId;
        const testerId = req.body.testerId;
        const userId = req.body.userId;
        const optionNumber = req.body.optionNumber;
        let conditions = {
            userId,
            testerId,
            trialId
        };
        try {
        const extractOption = await ScheduleConversation.find(conditions);
        const dateValue = extractOption[0].suggestedOptions[`${optionNumber}`];
        let update = {
            confirmedTimeStatus: true,
            confirmedTime:dateValue
        };
            ScheduleConversation.findOneAndUpdate(conditions, update).exec();
        } catch(err) {
            res.status(422).send(err);
        }
    });

    app.get('/api/match/list', async (req,res) => {
       const userId = req.user._id;
       const userName = req.user.displayName;
       const finalObj = {};
       finalObj.selfDetails = {userId,userName};
       const userMatches = {};
       const testerMatches = {};


        try {
            // Get Matched Trial IDs as Tester from existing match schedule records.
            const trialsAsTester = await ScheduleConversation.find({testerId:userId, confirmedTimeStatus:true });
            const trialsTesterIds = _.map(trialsAsTester, ({trialId}) => {
                return trialId;
            });
            const interimTesterTrials = await Trial.find({
                _id: {
                    $in: trialsTesterIds
                }
            });

            finalObj.testerTrials = _.map(interimTesterTrials, (item) => {
                let trial = _.find(trialsAsTester,{trialId:item._id });
                const newAddition = { timeToSpeak:trial.confirmedTime, otherPartyId:trial.userId.toString()};
                testerMatches[`${item._id}`] = newAddition;

                return _.extend({}, item, newAddition);
            });

            // Get Matched Trial IDs as User from existing match schedule records.
            const trialsAsUser = await ScheduleConversation.find({userId, confirmedTimeStatus: true});
            const trialsIds = _.map(trialsAsUser, ({ trialId }) => {
                return trialId;
            });

            const interimTrials =  await Trial.find({
                _id: {
                    $in: trialsIds
                }
            });

            finalObj.userTrials = _.map(interimTrials, (item) => {
                let trial = _.find(trialsAsUser,{trialId:item._id });

                const newAddition = { timeToSpeak:trial.confirmedTime, otherPartyId:trial.testerId.toString()};
                userMatches[`${item._id}`] = newAddition;

                return _.extend({},item, newAddition);
            });


             for (let key in userMatches) {
                let userObj = await User.find({_id:userMatches[key].otherPartyId });
                 userMatches[key].otherPartyName = userObj[0].displayName;
             }
            for (let key in testerMatches) {
                let testerObj = await User.find({_id:testerMatches[key].otherPartyId });
                testerMatches[key].otherPartyName = testerObj[0].displayName;
            }

            res.send({finalObj,userMatches,testerMatches});
        } catch(err) {
            res.status(422).send(err);
        }
    });

    };