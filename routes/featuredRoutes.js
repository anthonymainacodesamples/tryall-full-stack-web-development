const RandomTryalls = require('../services/cronJobs');
const logger = require('../services/logger');
const mongoose = require('mongoose');
const Trial = mongoose.model('tryallcompanies');

let featuredTrials15 = [];
let featuredTrials30 = [];

var getSummarized =  async function (summarizedBooleanValue) {

  let tryalls;

  if (summarizedBooleanValue) {
    tryalls = RandomTryalls.get15randomTryalls;
  } else {
    tryalls = RandomTryalls.get30randomTryalls;
  }

  // console.log(`15 trials length: ${featuredTrials15.length}`);

  // Forcefully get random trials if cronJob hasn't run yet
  if (tryalls.length === 0) {
    // console.log(`random tryalls length is 0`);

    if (summarizedBooleanValue) {
      if (featuredTrials15.length !== 0) {
        // console.log("returning 15 saved");
        return featuredTrials15
      }
    } else {
      if (featuredTrials30.length !== 0) {
        // console.log("returning 30 saved");
        return featuredTrials30
      }
    }

    try {
      let trials = await Trial.aggregate(
        [
          {
            $match: {
              tagLine: {
                $ne: ""
              },
              description: {
                $ne: "",
                $exists: true
              },
              monthlyDollarPrice: {
                $nin: [0, 0.1, "0", "0.1"]
              },
              DurationInDays: {
                $ne: 0
              }
            }
          },
          {
            $sample: {
              size: summarizedBooleanValue ? 15 : 30
            }
          }
        ]);

      if (summarizedBooleanValue) {
        featuredTrials15 = trials;
      } else {
        featuredTrials30 = trials;
      }
      return trials;
    } catch (err) {
      logger.error(`error getting random trials: ${err}`);
    }
  } else {
    return tryalls
  }
};

module.exports = (app) => {

  app.get('/api/featured', async (req, res) => {

    //Summarized Boolean value to determine if it needs 15 or 30
    let summarizedBooleanValue = req.query.summarizedVal;
    let bool = summarizedBooleanValue === 'true';

    getSummarized(bool)
      .then(toReturn => {
      res.send(toReturn);
    });
  });

};

