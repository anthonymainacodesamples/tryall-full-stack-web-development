/**
 * Created by AnthonyMaina on 10/3/17.
 */

module.exports = {
    googleClientID:process.env.GOOGLE_CLIENT_ID,
    googleClientSecret:process.env.GOOGLE_CLIENT_SECRET,
    mongoURI:process.env.MONGO_URI,
    cookieKey:process.env.COOKIE_KEY,
    iosClientID:process.env.IOS_CLIENT_ID,
    sendGridKey:process.env.SEND_GRID_KEY,
    redirectDomain:process.env.REDIRECT_DOMAIN,
    matchingApiURL:process.env.MATCHING_API
};
